https://www.codeproject.com/Articles/153716/WS-Enumeration-for-WCF-WF

Introduction
This article could be considered as a continuation of the WS-Transfer Service for Workflow article written by Roman Kiss. I've implemented the WS-Enumeration spec based on the approach suggested by Roman Kiss. Utilizing the sequence workflows for service or protocols development attracts me since it is a very natural way to do it. In many cases, you need to just look at the scheme in order to understand how the service or protocol operates.

The purpose of WS-Enumeration is to enumerate the elements in different structures (it could be a list, tree etc.). So one may suppose that LINQ could be helpful for it. Thinking of it, I recall the LINQ to Tree - A Generic Technique for Querying Tree-like Structures article written by Colin Eberhardt. Colin's main idea was to apply the "LINQ to XML" technique to non-XML tree structures. Colin developed the ILinqToTree<T> adapter interface and a set of extension methods which allows to execute queries in a unified way for different tree structures whether it is a file system or a database or something else. Since it is right what I need for WS-Enumeration, I utilized Colin's approach here.

