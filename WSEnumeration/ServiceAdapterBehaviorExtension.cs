//*****************************************************************************
//    Description.....Virtual Adapter 
//                    - loosely coupled service behavior extension
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)         
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    04/04/06    Roman Kiss     transaction option
//    07/07/06    Roman Kiss     migrating to JuneCTP 
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Configuration;
using System.Reflection;
#endregion

namespace WSEnumeration
{

    /// <summary>
    /// Empty (Null) class
    /// </summary>
    public class ConfiguredAddapter { }

    
    /// <summary>
    /// Virtual Adapter - layer between the service and physical adapter
    /// </summary>
    /// <typeparam name="ServiceAdapter">Adapter class</typeparam>
    /// <typeparam name="AdapterInterface">Interface to Adapter</typeparam>
    public class ServiceAdapterBase<ServiceAdapter, AdapterInterface> where ServiceAdapter : class
    {
        #region Properties
        private HybridDictionary _properties;
        private AdapterInterface _adapter;
        /// <summary>
        /// 
        /// </summary>
        public AdapterInterface Adapter
        {
            get { return (AdapterInterface)_adapter; }
            set { _adapter = value; }
        }
        public HybridDictionary Properties
        {
            get { return _properties; }
        }
        #endregion

        public ServiceAdapterBase()
        {
            ServiceHost host = OperationContext.Current.Host as ServiceHost;
            if (host.Description.Behaviors.Contains(typeof(AdapterAttribute)))
            {
                AdapterAttribute aa = host.Description.Behaviors.Find<AdapterAttribute>();
                _properties = aa.Parameters;
                if (typeof(ServiceAdapter) != typeof(ConfiguredAddapter))
                {
                    // tightly coupled adapter (compiler)
                    Adapter = (AdapterInterface)Activator.CreateInstance(typeof(ServiceAdapter), new object[] { Properties });
                }
                else if (aa.AdapterType != null)
                {
                    // loosely coupled adapter (config file)
                    Adapter = (AdapterInterface)Activator.CreateInstance(aa.AdapterType, new object[] { Properties });
                }
                else
                    throw new FaultException<ConfigurationErrorsException>(new ConfigurationErrorsException("Can't create Adapter"));
            }
        }
    }


    #region Attribute
    //[AttributeUsage(AttributeTargets.Method)]
    public class AdapterAttribute : Attribute, IOperationBehavior, IEndpointBehavior, IServiceBehavior
    {
        #region Properties
        HybridDictionary _parameters;
        private Type _adapterType;
        public string Name
        {
            get { return Parameters["name"] as string; }
            set { Parameters.Add("name", value); }
        }
        public Type AdapterType
        {
            get { return _adapterType; }
            set { _adapterType = value; }
        }
        public HybridDictionary Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
        #endregion

        #region Constructors
        public AdapterAttribute()
        {
            _parameters = new HybridDictionary();
        }
        public AdapterAttribute(Type adapterType, HybridDictionary parameters)
        {
            _adapterType = adapterType;
            _parameters = parameters;
        }
        #endregion

        #region IOperationBehavior Members
        public void ApplyDispatchBehavior(OperationDescription description, DispatchOperation dispatch)
        {
            if (dispatch.Invoker is AdapterInvoker)
            {
                return;
            }
            dispatch.Invoker = new AdapterInvoker(Name, AdapterType, Parameters, dispatch.Invoker);

            // option: transaction scope
            string txscope = Parameters["TransactionScopeRequired"] as string;
            if (txscope != null)
            {
                string autocomplete = Parameters["TransactionAutoComplete"] as string;
                dispatch.TransactionRequired = System.Convert.ToBoolean(txscope);
                if (autocomplete != null)
                    dispatch.TransactionAutoComplete = System.Convert.ToBoolean(autocomplete);
            }
        }
        public void AddBindingParameters(OperationDescription description, BindingParameterCollection parameters){}
        public void ApplyClientBehavior(OperationDescription description, ClientOperation proxy){}
        public void Validate(OperationDescription description){}
        #endregion

        #region IServiceBehavior Members
        public void ApplyDispatchBehavior(ServiceDescription description, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher cd in serviceHostBase.ChannelDispatchers)
            {
                foreach (EndpointDispatcher epdispatch in cd.Endpoints)
                {
                    ApplyDispatchBehavior(null, epdispatch);  
                }
            }
        }
        public void AddBindingParameters(ServiceDescription description, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection parameters)
        {
        }
        public void Validate(ServiceDescription description, ServiceHostBase serviceHostBase){}
        #endregion

        #region IEndpointBehavior Members
        public void ApplyDispatchBehavior(ServiceEndpoint serviceEndpoint, EndpointDispatcher endpointDispatcher)
        {
            foreach (DispatchOperation dispatch in endpointDispatcher.DispatchRuntime.Operations)
            {
                ApplyDispatchBehavior(null, dispatch);
            }
        }
        public void ApplyClientBehavior(ServiceEndpoint serviceEndpoint, ClientRuntime behavior) {}
        public void Validate(ServiceEndpoint serviceEndpoint) {}
        public void AddBindingParameters(ServiceEndpoint serviceEndpoint, BindingParameterCollection bindingParameters){}
        #endregion
    }
    #endregion

    #region Invoker
    public class AdapterInvoker : IOperationInvoker
    {
        #region Private Members
        string _name;
        Type _adapterType;
        IOperationInvoker innerOperationInvoker;
        HybridDictionary _parameters;
        #endregion

        public string Name
        {
            get { return _name; }
        }
        public Type AdapterType
        {
            get { return _adapterType; }
        }
        public HybridDictionary Parameters
        {
            get { return _parameters; }
        }

        #region Constructors
        public AdapterInvoker()
        {
        }
        public AdapterInvoker(string name, Type type, HybridDictionary parameters, IOperationInvoker innerOperationInvoker)
        {
            this._name = name;
            this._adapterType = type;
            this._parameters = parameters;
            this.innerOperationInvoker = innerOperationInvoker;
        }
        #endregion

        #region IOperationInvoker Members
        object[] IOperationInvoker.AllocateInputs()
        {
            return this.innerOperationInvoker.AllocateInputs();
        }

        object IOperationInvoker.Invoke(object instance, object[] inputs, out object[] outputs)
        {
            string name = (string)_parameters["name"];
            PropertyInfo pi = instance.GetType().GetProperty("Adapter");
            object adapter = pi.GetValue(instance, null);
            if (adapter == null && AdapterType != null)
            {
                pi.SetValue(instance, Activator.CreateInstance(this.AdapterType, new object[] { this.Parameters }), null);
            }
            object retval = this.innerOperationInvoker.Invoke(instance, inputs, out outputs);
            return retval;
        }

        IAsyncResult IOperationInvoker.InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            return this.innerOperationInvoker.InvokeBegin(instance, inputs, callback, state);
        }

        object IOperationInvoker.InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            return this.innerOperationInvoker.InvokeEnd(instance, out outputs, result);
        }

        bool IOperationInvoker.IsSynchronous
        {
            get { return this.innerOperationInvoker.IsSynchronous; }
        }
        #endregion
    }
    #endregion

    #region AdapterBehaviorExtension
    /// <summary>
    /// Configuration class
    /// </summary>
    public class ServiceAdapterBehaviorElement : BehaviorExtensionElement
    {
        HybridDictionary _unrecognizedAttributes = new HybridDictionary(true);

        #region Constructor(s)
        public ServiceAdapterBehaviorElement()
        {
        }
        #endregion

        #region ConfigurationProperties
        [ConfigurationProperty("type", IsRequired=true)]
        public string AdapterTypeName
        {
            get { return (string)base["type"]; }
            set { base["type"] = value; }
        }
        #endregion

        #region BehaviorExtensionElement
        protected override object CreateBehavior()
        {
            string type = (string)this["type"];
            Type adapterType = (type != null && type.Trim().Length > 0) ? Type.GetType(type) : null;
            AdapterAttribute adapter = new AdapterAttribute(adapterType, _unrecognizedAttributes);
            return adapter;
        }
        public override void CopyFrom(ServiceModelExtensionElement from)
        {
            base.CopyFrom(from);
            ServiceAdapterBehaviorElement section = (ServiceAdapterBehaviorElement)from;
            this.AdapterTypeName = section.AdapterTypeName;
            this._unrecognizedAttributes = section._unrecognizedAttributes;
        }
        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                ConfigurationPropertyCollection collection = new ConfigurationPropertyCollection();
                collection.Add(new ConfigurationProperty("type", typeof(string), ""));
                return collection;
            }
        }
        public override Type BehaviorType
        {
            get { return typeof(AdapterAttribute); }
        }
        protected override bool OnDeserializeUnrecognizedAttribute(string name, string value)
        {
            _unrecognizedAttributes.Add(name, value);
            return true;
        }
        #endregion
    }
    #endregion
}