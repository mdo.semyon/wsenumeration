//*****************************************************************************
//    Description.....Message wrapper
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)        
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    04/04/06    Roman Kiss     XmlToObject
//    07/07/06    Roman Kiss     migrating to JuneCTP 
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
#endregion


namespace WSEnumeration
{
    public sealed class Convert
    {
        private Convert() { }
        public static T XmlToObject<T>(object resource)
        {
            if (resource is XmlElement && typeof(T) != typeof(object))
            {
                XmlElement element = resource as XmlElement;
                XmlSerializer xs = new XmlSerializer(typeof(T));
                using (MemoryStream ms = new MemoryStream())
                {
                    XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.UTF8);
                    element.WriteTo(xtw);
                    xtw.Flush();
                    ms.Position = 0;
                    return (T)xs.Deserialize(ms);
                }
            }
            else
            {
                return (T)resource;
            }
        }

        /// <summary>
        /// Serialize object into the XmlElement
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        public static XmlElement ObjectToXml(object resource)
        {
            if (resource != null)
            {
                if (resource is XmlElement)
                {
                    return resource as XmlElement;
                }
                else if (resource is XmlNode)
                {
                    return resource as XmlElement;
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        XmlWriter xtw = XmlTextWriter.Create(ms, null);
                        XmlSerializer xs = new XmlSerializer(resource.GetType());
                        xs.Serialize(xtw, resource);
                        ms.Position = 0;
                        doc.Load(ms);
                    }
                    return doc.DocumentElement;
                }
            }
            return null;
        }

        /// <summary>
        /// Serialize object into the xml wrapper
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        public static XmlElement ObjectToXmlAnyElement(object resource)
        {
            if (resource != null)
            {
                if (resource is XmlElement)
                {
                    return resource as XmlElement;
                }
                else if (resource is XmlNode)
                {
                    return resource as XmlElement;
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        XmlWriter xtw = XmlTextWriter.Create(ms, null);
                        xtw.WriteStartElement("root");
                        XmlSerializer xs = new XmlSerializer(resource.GetType());
                        xs.Serialize(xtw, resource);
                        xtw.WriteEndElement();
                        xtw.Flush();
                        ms.Position = 0;
                        doc.Load(ms);                                           
                    }
                    return doc.DocumentElement;
                }
            }
            return null;
        }
    }
}
