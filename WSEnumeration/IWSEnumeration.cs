//*****************************************************************************
//    Description.....WS-Enumeration Service Contract
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)    
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    07/07/06    Roman Kiss     migrating to JuneCTP 
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using f = WSEnumeration.Faults;
using a = WSEnumeration.Adapters;

#endregion

namespace WSEnumeration
{
    /// <summary>
    /// WS-Enumeration Interface
    /// </summary>
    [ServiceContract(Namespace = WSEnumeration.NamespaceUri)]
    public interface IWSEnumeration : IWSEnumerationFactory, IWSEnumerationOperation
    {
    }
    /// <summary>
    /// WS-Enumeration Operation Interface
    /// </summary>
    [ServiceContract(Namespace = WSEnumeration.NamespaceUri)]
    public interface IWSEnumerationOperation
    {
        [OperationContract(Action = WSEnumeration.PullAction, ReplyAction = WSEnumeration.PullResponseAction)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        //[XmlSerializerFormat]
        PullResponse Pull(PullRequest request);
        
        [OperationContract(Action = WSEnumeration.RenewAction, ReplyAction = WSEnumeration.RenewResponseAction)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [XmlSerializerFormat]
        RenewResponse Renew(RenewRequest request);

        [OperationContract(Action = WSEnumeration.GetStatusAction, ReplyAction = WSEnumeration.GetStatusResponseAction)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [XmlSerializerFormat]
        GetStatusResponse GetStatus(GetStatusRequest request);

        [OperationContract(Action = WSEnumeration.ReleaseAction, ReplyAction = WSEnumeration.ReleaseResponseAction)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [XmlSerializerFormat]
        ReleaseResponse Release(ReleaseRequest request);
    }
    /// <summary>
    /// WS-Enumeration Factory Interface
    /// </summary>
    [ServiceContract(Namespace = WSEnumeration.NamespaceUri)]
    public interface IWSEnumerationFactory
    {
        [OperationContract(Action = WSEnumeration.EnumerateAction, ReplyAction = WSEnumeration.EnumerateResponseAction)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(f.InvalidExpirationTimeException))]
        [FaultContract(typeof(f.UnsupportedExpirationTypeException))]
        [FaultContract(typeof(f.FilteringNotSupportedException))]
        [FaultContract(typeof(f.FilterDialectRequestedUnavailableException))]
        [FaultContract(typeof(f.CannotProcessFilterException))]
        [XmlSerializerFormat]
        EnumerateResponse Enumerate(EnumerateRequest request);
    }

}