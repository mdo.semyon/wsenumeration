﻿//*****************************************************************************
//    Description.....WS-Enumeration Release Request/Response DataContract
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
#endregion

namespace WSEnumeration.DataContract
{
    [XmlRoot(ElementName = "Release", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = Release.DataContractName, Namespace = Release.DataContractNamespace)]
    [Serializable]
    public class Release
    {
        #region constants
        public const string DataContractName = "Release";
        public const string DataContractNamespace = WSEnumeration.NamespaceUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        #region DataMembers
        string _ctx;
        #endregion

        #region Properties
        [DataMember(Order = 0)]public string EnumerationContext { get { return _ctx; } set { _ctx = value; } }
        #endregion

        #region Constructors
        public Release() { }
        public Release(Release e)
        {
            if (e.EnumerationContext != null)
                _ctx = string.Copy(e.EnumerationContext);
        }
        #endregion

    }
}
