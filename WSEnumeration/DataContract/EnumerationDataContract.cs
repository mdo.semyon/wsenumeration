//*****************************************************************************
//    Description.....Example(s) of the DataContract for WS-Transfer Messages
//                                                 
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)         
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    04/04/06    Roman Kiss     making classes Serializable + more properties
//    07/07/06    Roman Kiss     migrating to JuneCTP 
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
#endregion

namespace WSEnumeration.Adapters
{
    [XmlRoot(ElementName = "Enumerate", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = Enumerate.DataContractName, Namespace = Enumerate.DataContractNamespace)]
    [Serializable]
    public class Enumerate
    {
        #region constants
        public const string DataContractName = "Enumerate";
        public const string DataContractNamespace = "http://schemas.xmlsoap.org/ws/2004/09/enumeration";
        public static XmlQualifiedName XmlQualifiedName
        {
            get { return new XmlQualifiedName(DataContractName, DataContractNamespace); }
        }
        #endregion

        #region DataMembers
        string _filter;
        #endregion

        #region Properties
        [DataMember(Order = 0)]
        public string Filter
        {
            get { return _filter; }
            set { _filter = value; }
        }
        #endregion

        #region Constructors
        public Enumerate() { }
        public Enumerate(string filter) { _filter = filter; }
        public Enumerate(Enumerate e)
        {
            if (e != null)
            {
                _filter = e.Filter;
            }
        }
        #endregion

    }

    [XmlRoot(ElementName = "EnumerateResponse", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = EnumerateResponse.DataContractName, Namespace = EnumerateResponse.DataContractNamespace)]
    [Serializable]
    public class EnumerateResponse
    {
        #region constants
        public const string DataContractName = "EnumerateResponse";
        public const string DataContractNamespace = "http://schemas.xmlsoap.org/ws/2004/09/enumeration";
        public static XmlQualifiedName XmlQualifiedName
        {
            get { return new XmlQualifiedName(DataContractName, DataContractNamespace); }
        }
        #endregion

        #region DataMembers
        string _ctx;
        #endregion

        #region Properties
        [DataMember(Order = 0)]
        public string EnumerationContext
        {
            get { return _ctx; }
            set { _ctx = value; }
        }
        #endregion

        #region Constructors
        public EnumerateResponse() {}
        public EnumerateResponse(string ctx) { _ctx = ctx; }
        public EnumerateResponse(EnumerateResponse e)
        {
            if (e.EnumerationContext != null)
                _ctx = string.Copy(e.EnumerationContext);
        }
        #endregion

    }

    [XmlRoot(ElementName = "Pull", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = Pull.DataContractName, Namespace = Pull.DataContractNamespace)]
    [Serializable]
    public class Pull
    {
        #region constants
        public const string DataContractName = "Pull";
        public const string DataContractNamespace = "http://schemas.xmlsoap.org/ws/2004/09/enumeration";
        public static XmlQualifiedName XmlQualifiedName
        {
            get { return new XmlQualifiedName(DataContractName, DataContractNamespace); }
        }
        #endregion

        #region DataMembers
        string _ctx;
        int _maxElements;
        #endregion

        #region Properties
        [DataMember(Order = 0)]
        public string EnumerationContext
        {
            get { return _ctx; }
            set { _ctx = value; }
        }
        [DataMember(Order = 1)]
        public int MaxElements
        {
            get { return _maxElements; }
            set { _maxElements = value; }
        }
        #endregion

        #region Constructors
        public Pull() { }
        public Pull(Pull e)
        {
            if (e.EnumerationContext != null)
                _ctx = string.Copy(e.EnumerationContext);
            if (e.MaxElements != null)
                _maxElements = e.MaxElements;
        }
        #endregion

    }

    [XmlSchemaProvider(null, IsAny = true)]
    [XmlRoot(ElementName = "PullResponse", Namespace = WSEnumeration.NamespaceUri, IsNullable = true)]
    [Serializable]
    public class PullResponse : IXmlSerializable
    {
        #region DataMembers
        string _ctx;
        IXmlSerializable _items;
        #endregion

        #region Properties
        public string EnumerationContext
        {
            get { return _ctx; }
            set { _ctx = value; }
        }
        public IXmlSerializable Items
        {
            get { return _items; }
            set { _items = value; }
        }
        #endregion

        #region Constructors
        public PullResponse() : this(null, null) 
        {
            Items = new WSEnumerationWorkflow.FileSystem.ItemsDescriptor();
        }
        public PullResponse(string ctx) : this(ctx, null) 
        {
            Items = new WSEnumerationWorkflow.FileSystem.ItemsDescriptor();
        }
        public PullResponse(string ctx, IXmlSerializable items)
        {
            _ctx = ctx;
            _items = items;
        }
        #endregion

        #region IXmlSerializable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("PullResponse", WSEnumeration.NamespaceUri);
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                if (reader.IsStartElement("EnumerationContext", WSEnumeration.NamespaceUri))
                    EnumerationContext = reader.ReadElementString("EnumerationContext", WSEnumeration.NamespaceUri);

                if (reader.IsStartElement("Items", WSEnumeration.NamespaceUri))
                    Items.ReadXml(reader);

                reader.MoveToContent();
            }
       }
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PullResponse", WSEnumeration.NamespaceUri);
            
            if (!String.IsNullOrEmpty(EnumerationContext))
                writer.WriteElementString("EnumerationContext", WSEnumeration.NamespaceUri);

            if(Items != null)
                Items.WriteXml(writer);    

            writer.WriteEndElement();
        }
        #endregion
    }
}
