//*****************************************************************************
//    Description.....WS-Enumeration Enumerate Request/Response DataContract
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
#endregion

namespace WSEnumeration.DataContract
{
    [XmlRoot(ElementName = "Enumerate", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = Enumerate.DataContractName, Namespace = Enumerate.DataContractNamespace)]
    [Serializable]
    public class Enumerate
    {
        #region constants
        public const string DataContractName = "Enumerate";
        public const string DataContractNamespace = WSEnumeration.NamespaceUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        #region DataMembers
        string          _endTo;
        string          _expires;
        string          _filter;
        string          _dialect;
        #endregion

        #region Properties
        [DataMember(Order = 0)]public string            EndTo   { get { return _endTo;      } set { _endTo      = value; } }
        [DataMember(Order = 1)]public string            Expires { get { return _expires;    } set { _expires    = value; } }
        [DataMember(Order = 2)]public string            Filter  { get { return _filter;     } set { _filter     = value; } }
        [DataMember(Order = 2)]public string            Dialect { get { return _dialect;    } set { _dialect    = value; } }
        #endregion

        #region Constructors
        public Enumerate() { }
        public Enumerate(Enumerate e)
        {
            if (e.EndTo != null)
                _endTo = e._endTo;
            if (e.Filter != null)
                _filter = string.Copy(e.Filter);
            _expires = e.Expires;
        }
        #endregion
    }

    [XmlRoot(ElementName = "EnumerateResponse", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = EnumerateResponse.DataContractName, Namespace = EnumerateResponse.DataContractNamespace)]
    [Serializable]
    public class EnumerateResponse
    {
        #region constants
        public const string DataContractName = "EnumerateResponse";
        public const string DataContractNamespace = WSEnumeration.NamespaceUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        #region DataMembers
        DateTime    _expires;
        string      _ctx;
        #endregion

        #region Properties
        [DataMember(Order = 0)]public DateTime  Expires             { get { return _expires;    } set { _expires    = value; } }
        [DataMember(Order = 1)]public string    EnumerationContext  { get { return _ctx;        } set { _ctx        = value; } }
        #endregion

        #region Constructors
        public EnumerateResponse() { }
        public EnumerateResponse(EnumerateResponse e)
        {
            if (e.EnumerationContext != null)
                _ctx = string.Copy(e.EnumerationContext);
            _expires = e.Expires;
        }
        #endregion
    }
}

