//*****************************************************************************
//    Description.....WS-Enumeration Pull Request/Response DataContract
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    24/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    24/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;

using WSEnumeration.Descriptors;
#endregion

namespace WSEnumeration.DataContract
{
    [XmlRoot(ElementName = "Pull", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = Pull.DataContractName, Namespace = Pull.DataContractNamespace)]
    [Serializable]
    public class Pull
    {
        #region constants
        public const string DataContractName = "Pull";
        public const string DataContractNamespace = WSEnumeration.NamespaceUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        #region DataMembers
        string  _ctx;
        int     _maxTime;
        int     _maxElements;
        int     _maxCharacters;
        #endregion

        #region Properties
        [DataMember(Order = 0)]public string    EnumerationContext  { get { return _ctx;            } set { _ctx            = value; } }
        [DataMember(Order = 1)]public int       MaxTime             { get { return _maxTime;        } set { _maxTime        = value; } }
        [DataMember(Order = 2)]public int       MaxElements         { get { return _maxElements;    } set { _maxElements    = value; } }
        [DataMember(Order = 3)]public int       MaxCharacters       { get { return _maxCharacters;  } set { _maxCharacters  = value; } }
        #endregion

        #region Constructors
        public Pull() { }
        public Pull(Pull e)
        {
            if (e.EnumerationContext != null)
                _ctx = string.Copy(e.EnumerationContext);

            _maxTime        = e.MaxTime;
            _maxElements    = e.MaxElements;
            _maxCharacters  = e.MaxCharacters;
        }
        #endregion

    }

    [XmlSchemaProvider(null, IsAny = true)]
    //[XmlRoot(ElementName = "PullResponse", Namespace = WSEnumeration.NamespaceUri, IsNullable = true)]
    [Serializable]
    public class PullResponse : IXmlSerializable
    {
        #region DataMembers
        string              _ctx;
        IXmlSerializable    _items;
        #endregion

        #region Properties
        public string           EnumerationContext  { get { return _ctx;    } set { _ctx    = value; } }
        public IXmlSerializable Items               { get { return _items;  } set { _items  = value; } }
        #endregion

        #region Constructors
        public PullResponse() {}
        public PullResponse(string ctx, IXmlSerializable items) 
        {
            _ctx = ctx;
            _items = items;
        }
        #endregion

        #region IXmlSerializable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            string strType = default(string);

            //reader.ReadStartElement("PullResponse", WSEnumeration.NamespaceUri);
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                if (reader.IsStartElement("EnumerationContext"))
                    EnumerationContext = reader.ReadElementString("EnumerationContext");

                if (reader.IsStartElement("Items"))
                {
                    if (reader.HasAttributes)
                        while (reader.MoveToNextAttribute())
                            if (reader.LocalName == "Type")
                                strType = reader.Value;
                    reader.MoveToElement();

                    if (reader.IsEmptyElement) { reader.Skip(); break; }

                    if (String.IsNullOrEmpty(strType))
                        throw new ArgumentNullException("Element Items, attribute Type is empty.");

                    Type type = Type.GetType("WSEnumeration.ItemDescriptorCollection`1[[" + strType + "]]");
                    if (type == null)
                        throw new ArgumentException("Attempt to create type from value of //Items/@Type failure."); 

                    Items = Activator.CreateInstance(type) as IXmlSerializable;
                    Items.ReadXml(reader);
                }

                reader.MoveToContent();
            }
        }
        public void WriteXml(XmlWriter writer)
        {
            //writer.WriteStartElement("PullResponse", WSEnumeration.NamespaceUri);

            if (!String.IsNullOrEmpty(EnumerationContext))
                writer.WriteElementString("EnumerationContext", EnumerationContext);

            if (Items != null)
                Items.WriteXml(writer);

            //EndOfSequence

            //writer.WriteEndElement();
        }
        #endregion
    }
}