﻿//*****************************************************************************
//    Description.....WS-Enumeration EnumerationEnd Request/Response DataContract
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
#endregion

namespace WSEnumeration.DataContract
{
    [XmlRoot(ElementName = "EnumerationEnd", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = EnumerationEnd.DataContractName, Namespace = EnumerationEnd.DataContractNamespace)]
    [Serializable]
    public class EnumerationEnd
    {
        #region constants
        public const string DataContractName = "EnumerationEnd";
        public const string DataContractNamespace = WSEnumeration.NamespaceUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        #region DataMembers
        string  _ctx;
        string  _code;
        string  _reason;
        #endregion

        #region Properties
        [DataMember(Order = 0)]public string    EnumerationContext  { get { return _ctx;    } set { _ctx    = value; } }
        [DataMember(Order = 1)]public string    Code                { get { return _code;   } set { _code   = value; } }
        [DataMember(Order = 2)]public string    Reason              { get { return _reason; } set { _reason = value; } }
        #endregion

        #region Constructors
        public EnumerationEnd() { }
        public EnumerationEnd(EnumerationEnd e)
        {
            if (e.EnumerationContext != null)
                _ctx = string.Copy(e.EnumerationContext);

            _code = e.Code;

            if (e.Reason != null)
                _reason = string.Copy(e.Reason);
        }
        #endregion
    }
}
