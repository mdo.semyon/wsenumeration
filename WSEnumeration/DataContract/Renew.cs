﻿//*****************************************************************************
//    Description.....WS-Enumeration Renew Request/Response DataContract
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
#endregion

namespace WSEnumeration.DataContract
{
    [XmlRoot(ElementName = "Renew", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = Renew.DataContractName, Namespace = Renew.DataContractNamespace)]
    [Serializable]
    public class Renew
    {
        #region constants
        public const string DataContractName = "Renew";
        public const string DataContractNamespace = WSEnumeration.NamespaceUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        #region DataMembers
        DateTime    _expires;
        string      _ctx;
        #endregion

        #region Properties
        [DataMember(Order = 0)]public DateTime  Expires             { get { return _expires;    } set { _expires    = value; } }
        [DataMember(Order = 1)]public string    EnumerationContext  { get { return _ctx;        } set { _ctx        = value; } }
        #endregion

        #region Constructors
        public Renew() { }
        public Renew(Renew e)
        {
            if (e.EnumerationContext != null)
                _ctx = string.Copy(e.EnumerationContext);
            _expires = e.Expires;
        }
        #endregion

    }

    [XmlRoot(ElementName = "RenewResponse", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = RenewResponse.DataContractName, Namespace = RenewResponse.DataContractNamespace)]
    [Serializable]
    public class RenewResponse
    {
        #region constants
        public const string DataContractName = "RenewResponse";
        public const string DataContractNamespace = WSEnumeration.NamespaceUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        #region DataMembers
        DateTime    _expires;
        string      _ctx;
        #endregion

        #region Properties
        [DataMember(Order = 0)]public DateTime  Expires             { get { return _expires;    } set { _expires    = value; } }
        [DataMember(Order = 1)]public string    EnumerationContext  { get { return _ctx;        } set { _ctx        = value; } }
        #endregion

        #region Constructors
        public RenewResponse() { }
        public RenewResponse(RenewResponse e)
        {
            if (e.EnumerationContext != null)
                _ctx = string.Copy(e.EnumerationContext);
            _expires = e.Expires;
        }
        #endregion
    }
}
