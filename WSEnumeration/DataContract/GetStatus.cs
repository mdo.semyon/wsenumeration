﻿//*****************************************************************************
//    Description.....WS-Enumeration GetStatus Request/Response DataContract
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
#endregion

namespace WSEnumeration.DataContract
{
    [XmlRoot(ElementName = "GetStatus", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = GetStatus.DataContractName, Namespace = GetStatus.DataContractNamespace)]
    [Serializable]
    public class GetStatus
    {
        #region constants
        public const string DataContractName = "GetStatus";
        public const string DataContractNamespace = WSEnumeration.NamespaceUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        #region DataMembers
        string _ctx;
        #endregion

        #region Properties
        [DataMember(Order = 0)]public string EnumerationContext { get { return _ctx; } set { _ctx = value; } }
        #endregion

        #region Constructors
        public GetStatus() { }
        public GetStatus(GetStatus e)
        {
            if (e.EnumerationContext != null)
                _ctx = string.Copy(e.EnumerationContext);
        }
        #endregion

    }

    [XmlRoot(ElementName = "GetStatusResponse", Namespace = WSEnumeration.NamespaceUri)]
    [DataContract(Name = GetStatusResponse.DataContractName, Namespace = GetStatusResponse.DataContractNamespace)]
    [Serializable]
    public class GetStatusResponse
    {
        #region constants
        public const string DataContractName = "GetStatusResponse";
        public const string DataContractNamespace = WSEnumeration.NamespaceUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        #region DataMembers
        DateTime _expires;
        #endregion

        #region Properties
        [DataMember(Order = 0)]public DateTime Expires { get { return _expires; } set { _expires = value; } }
        #endregion

        #region Constructors
        public GetStatusResponse() { }
        public GetStatusResponse(GetStatusResponse e)
        {
            _expires = e.Expires;
        }
        #endregion
    }
}
