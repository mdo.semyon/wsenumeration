//*****************************************************************************
//    Description.....WS-Enumeration Service
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)         
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    07/07/06    Roman Kiss     migrating to JuneCTP
//    11/11/06    Roman Kiss     RTM 
//*****************************************************************************
//
#region References
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Xml;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
//
using dc = WSEnumeration.DataContract;
using f = WSEnumeration.Faults;

#endregion

namespace WSEnumeration
{
    /// <summary>
    /// WS-Enumeration Service Layer. This service will plug-in a loosely coupled business oriented Adapter. 
    /// </summary>
    public class WSEnumerationService : WSEnumerationService<ConfiguredAddapter> 
    { 
    }

    /// <summary>
    /// WS-Enumeration Service Layer. This service will plug-in a loosely coupled business oriented Adapter. 
    /// </summary>
    /// <typeparam name="T">Adapter class</typeparam>
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class WSEnumerationService<ServiceAdapter> : ServiceAdapterBase<ServiceAdapter, IWSEnumerationAdapter>, IWSEnumeration where ServiceAdapter : class
    {
        #region Constructors
        /// <summary>Default Constructor</summary>
        /// <remarks>Constructor has responsibility to intiate Adapter based on the CustomBinding properties</remarks>
        public WSEnumerationService()
        {           
        }
        #endregion

        #region IWSEnumeration Members
        public EnumerateResponse Enumerate(EnumerateRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            // Adapter action 
            string ctx = default(string);
            //try
            //{
                Adapter.Enumerate(request.ResourceUri, request.Enumerate.Expires, request.Enumerate.Filter, request.Enumerate.Dialect, out ctx);
            //}
            //catch (f.SourceShuttingDownException ex)
            //{
            //    return new EnumerateResponse(new dc.EnumerationEnd() { EnumerationContext = ctx, Code = WSEnumeration.SourceShuttingDownAction, Reason = "" });
            //}
            //catch (f.SourceCancellingException ex)
            //{
            //    return new EnumerateResponse(new dc.EnumerationEnd() { EnumerationContext = ctx, Code = WSEnumeration.SourceCancellingAction, Reason = "" });    
            //}

            // done
            return new EnumerateResponse(new dc.EnumerateResponse() { EnumerationContext = ctx });
        }
        public PullResponse Pull(PullRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            // Adapter action 
            string ctx = request.Pull.EnumerationContext;
            object items;

            Adapter.Pull(ctx, null, request.Pull.MaxElements, null, out items);

            IXmlSerializable obj = items as IXmlSerializable;
            if(obj == null)
                return new PullResponse(new dc.PullResponse(ctx, null));
            else
                return new PullResponse(new dc.PullResponse(ctx, obj));
        }
        public RenewResponse Renew(RenewRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            // Adapter action 
            string ctx = request.Renew.EnumerationContext;
            DateTime? expires;
            Adapter.Renew(ctx, null, out expires);

            // done
            return new RenewResponse(new dc.RenewResponse() { EnumerationContext = ctx, Expires = expires.Value});
        }
        public GetStatusResponse GetStatus(GetStatusRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            // Adapter action 
            string ctx = request.GetStatus.EnumerationContext;
            DateTime? expires;
            Adapter.GetStatus(ctx, out expires);

            // done
            return new GetStatusResponse(new dc.GetStatusResponse() { Expires = expires.Value });
        }
        public ReleaseResponse Release(ReleaseRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            // Adapter action 
            Adapter.Release(request.Release.EnumerationContext);

            // done
            return new ReleaseResponse();
        }
        #endregion
    }
}