﻿//*****************************************************************************
//    Description.....Example(s) of the DataContract for WS-Enumeration Messages
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    24/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    24/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using wse = WSEnumeration;
#endregion

namespace WSEnumeration
{
    [XmlSchemaProvider(null, IsAny = true)]
    [XmlRoot(ElementName = "Items", Namespace = wse.WSEnumeration.ServicebusUri, IsNullable = true)]
    [Serializable]
    public class ItemDescriptorCollection<T> : IXmlSerializable, IEnumerable<T> where T : IXmlSerializable
    {
        #region data
        IList<T> _descriptors { get; set; }
        public bool EndOfSequence { get; set; }
        #endregion

        #region .ctor
        public ItemDescriptorCollection() 
        {
            _descriptors = new List<T>();
        }
        #endregion

        #region public
        public void Add(T descriptor)
        {
            _descriptors.Add(descriptor);
        }
        #endregion

        #region IXmlSerializable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("Items", wse.WSEnumeration.ServicebusUri);

            while (reader.NodeType != XmlNodeType.EndElement)
            {
                T obj = (T)Activator.CreateInstance(typeof(T), new object[] { reader });
                _descriptors.Add(obj);

                reader.MoveToContent();
            }
            reader.ReadEndElement();

            if (reader.IsStartElement("EndOfSequence", wse.WSEnumeration.ServicebusUri))
            {
                EndOfSequence = true;
                reader.Skip();
            }
        }
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Items", wse.WSEnumeration.ServicebusUri);
            writer.WriteAttributeString("Type", typeof(T).FullName);

            foreach (T item in _descriptors)
                item.WriteXml(writer);
            
            writer.WriteEndElement();

            if (EndOfSequence)
            {
                writer.WriteStartElement("EndOfSequence", wse.WSEnumeration.ServicebusUri);
                writer.WriteEndElement();
            }
        }
        #endregion

        #region IEnumerable
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _descriptors.GetEnumerator();
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _descriptors.GetEnumerator();
        }
        public T this[int Index]
        {
            get
            {
                if (Index < 0 || Index >= _descriptors.Count)
                {
                    throw (new ArgumentOutOfRangeException("Descriptor index out of range"));
                }
                return _descriptors[Index];
            }
        }
        #endregion
    }
}
