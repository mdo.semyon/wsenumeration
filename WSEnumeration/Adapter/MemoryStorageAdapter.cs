//*****************************************************************************
//    Description.....Example of the WS-Transfer Service Adapter
//                  - Memory Storage
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)       
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    07/07/06    Roman Kiss     migrating to JuneCTP 
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Xml;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
#endregion

namespace WSEnumeration.Adapters
{
    /// <summary>
    /// Adapter for storing resource in the memory
    /// </summary>
    public class MemoryStorageAdapter : WSEnumerationAdapterBase<ResourceDescriptor>, IWSEnumerationAdapter
    {
        Hashtable _storage = null;
        string strStorageName = string.Empty;
        object SyncRoot = new object();
        public Hashtable Storage
        {
            get { return _storage; }
        }

        public MemoryStorageAdapter(HybridDictionary config)
        {
            base.Properties = config;
            strStorageName = base.Properties["name"] as string;
            if(strStorageName == null)
                throw new FaultException("Missing name of the Storage");

            lock (SyncRoot)
            {
                object storage = AppDomain.CurrentDomain.GetData(strStorageName);
                if (storage == null)
                {
                    storage = Hashtable.Synchronized(new Hashtable());
                    AppDomain.CurrentDomain.SetData(strStorageName, storage);
                }
                _storage = storage as Hashtable;
            }
        }

        public EndpointAddress Create(object resource)
        {
            ResourceDescriptor rd = new ResourceDescriptor();
            
            // action
            lock (SyncRoot)
            {
                Storage.Add(rd.Id, resource);
            }
            return this.ResourceEndpointAddress(null, rd);

            #region another way returning a resource identifier
            //List<AddressHeader> list = new List<AddressHeader>();
            //list.Add(AddressHeader.CreateAddressHeader(ResourceDescriptor.DataContractName, ResourceDescriptor.DataContractNamespace, rd));
            //return this.ResourceEndpointAddress(null, list);
            #endregion
        }

        public object Get(MessageHeaders resourceIdentifier)
        {
            ResourceDescriptor rd = GetResourceIdentifier(resourceIdentifier);

            #region another way getting a resource identifier
            //ResourceDescriptor rd = GetResourceIdentifier<ResourceDescriptor>(resourceIdentifier, ResourceDescriptor.XmlQualifiedName);
            #endregion

            // action
            lock (SyncRoot)
            {
                if (Storage.Contains(rd.Id) == true)
                {
                    return Storage[rd.Id];
                }
            }
            throw new Exception(string.Format("The Resource [rid={0}] doesn't exist in the storage", rd.Id));
        }

        public object Put(MessageHeaders resourceIdentifier, object resource)
        {
            ResourceDescriptor rd = GetResourceIdentifier(resourceIdentifier);
 
            // action
            lock (SyncRoot)
            {
                if (Storage.Contains(rd.Id) == true)
                {
                    Storage[rd.Id] = resource;
                    return null;
                }
            }
            throw new FaultException(string.Format("The Resource [rid={0}] doesn't exist in the storage", rd.Id));
        }

        public void Delete(MessageHeaders resourceIdentifier)
        {
            ResourceDescriptor rd = GetResourceIdentifier(resourceIdentifier);

            // action
            lock (SyncRoot)
            {
                if (Storage.Contains(rd.Id) == true)
                {
                    Storage.Remove(rd.Id);
                    return;
                }
            }
            throw new FaultException(string.Format("The Resource [rid={0}] doesn't exist in the storage", rd.Id));
        }
    }
}












