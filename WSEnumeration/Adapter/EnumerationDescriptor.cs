﻿#region References
using System;
using System.Collections;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
#endregion

namespace WSEnumeration.Adapters
{
    [XmlRoot(ElementName = "EnumerationDescriptor", Namespace = WSEnumeration.ServicebusUri)]
    [DataContract(Name = EnumerationDescriptor.DataContractName, Namespace = EnumerationDescriptor.DataContractNamespace)]
    [Serializable]
    public class EnumerationDescriptor
    {
        #region constants
        public const string DataContractName = "EnumerationDescriptor";
        public const string DataContractNamespace = WSEnumeration.ServicebusUri;
        public static XmlQualifiedName XmlQualifiedName { get { return new XmlQualifiedName(DataContractName, DataContractNamespace); } }
        #endregion

        [DataMember(Order = 0)]public string    ResourceUri         { get; set; }
        [DataMember(Order = 1)]public string    Expires             { get; set; }
        [DataMember(Order = 2)]public string    Filter              { get; set; }
        [DataMember(Order = 3)]public string    Dialect             { get; set; }
        [DataMember(Order = 4)]public string    EnumerationContext  { get; set; }
        [DataMember(Order = 5)]public int       MaxTime             { get; set; }
        [DataMember(Order = 6)]public int       MaxElements         { get; set; }
        [DataMember(Order = 7)]public int       MaxCharacters       { get; set; }

        #region Constructors
        public EnumerationDescriptor() 
        {
            ResourceUri         = default(string);
            Expires             = default(string);
            EnumerationContext  = default(string);
            Filter              = default(string);
            Dialect             = default(string);
        }
        #endregion
    }
}
