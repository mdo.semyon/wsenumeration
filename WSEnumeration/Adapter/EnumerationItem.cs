﻿//*****************************************************************************
//    Description.....Example(s) of the DataContract for WS-Enumeration Messages
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    24/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    24/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
#endregion

namespace WSEnumeration.Adapters
{
    public class EnumerationItem : IXmlSerializable
    {
        #region data
        public object Resource { get; set; }
        #endregion

        #region .ctor
        public EnumerationItem() {}
        public EnumerationItem(XmlReader reader)
        {
            this.ReadXml(reader);
        }
        #endregion

        #region IXmlSerializable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            String type = String.Empty;

            if (reader.HasAttributes)
            {
                while (reader.MoveToNextAttribute())
                {
                    if (reader.LocalName == "Type")
                    {
                        type = reader.Value;
                    }
                }
                reader.MoveToElement();
            }

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement("Item", "http://www.rkiss.net/schemas/sb/2005/06/servicebus");

            if (!String.IsNullOrEmpty(type))
            {
                Resource = Activator.CreateInstance(Type.GetType(type));

                IXmlSerializable obj = Resource as IXmlSerializable;
                
                if (obj != null)
                    obj.ReadXml(reader);
            }
        }
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Item", WSEnumeration.NamespaceUri);
            if(Resource != null)
            {
                IXmlSerializable obj = Resource as IXmlSerializable;

                if (obj != null)
                    obj.WriteXml(writer);

                //XmlSerializer xs = new XmlSerializer(Resource.GetType());
                //xs.Serialize(writer, Resource);
            }
            writer.WriteEndElement();
        }
        #endregion
    }
}
