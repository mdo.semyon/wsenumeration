//*****************************************************************************
//    Description.....WS-Transfer Service Adapter for Workflow
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)       
//                        
//    Date Created:    05/05/06
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    05/05/06    Roman Kiss     Initial Revision
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Xml;
using System.Transactions;
//
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
//
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Runtime.Hosting;
//
using f = WSEnumeration.Faults;
using ServiceHostExtension;
#endregion

namespace WSEnumeration.Adapters
{
    /// <summary>
    /// Adapter for factory and operation of the resource in the workflow process
    /// </summary>
    public class WFAdapter : WSEnumerationAdapterBase<EnumerationDescriptor>, IWSEnumerationAdapter 
    {
        #region Private members
        string resourceTopic = null;
        int workflowTimeoutInSec = Defaults.Values.WorkflowTimeOut;
        WorkflowRuntime workflowRuntime = null;
        WSEnumerationLocalService WSEnumerationLocalService = null;
        #endregion

        #region Constructor
        public WFAdapter(HybridDictionary config)
        {
            // config file
            base.Properties = config;

            // resource topic
            resourceTopic = base.Properties[Defaults.Keys.Topic] as string;

            // response time from workflow
            if (int.TryParse(base.Properties[Defaults.Keys.WorkflowTimeOut] as string, out workflowTimeoutInSec) == false)
                workflowTimeoutInSec = Defaults.Values.WorkflowTimeOut;

            // follow to the workflow
            base.Properties["IncomingMessageHeaders"] = OperationContext.Current.IncomingMessageHeaders;
            base.Properties["IncomingMessageProperties"] = OperationContext.Current.IncomingMessageProperties;

            // Find the custom Indigio Extension on the Service Host by it's type
            WFServiceHostExtension extension = OperationContext.Current.Host.Extensions.Find<WFServiceHostExtension>();

            // Get a reference to the WorkflowRuntime
            this.workflowRuntime = extension.WorkflowRuntime;

            // Get a reference to the WSEnumerationLocalService
            this.WSEnumerationLocalService = (WSEnumerationLocalService)extension.ExternalDataExchangeService.GetService(typeof(WSEnumerationLocalService));

            // If the WSEnumerationLocalService is not registered with the runtime, then create an instance now and add it.  
            if (this.WSEnumerationLocalService == null)
            {
                this.WSEnumerationLocalService = new WSEnumerationLocalService(this.workflowRuntime);
                extension.ExternalDataExchangeService.AddService(this.WSEnumerationLocalService);
            }  
        }
        #endregion

        #region IWSEnumerationAdapter
        public void Enumerate(string uri, string expires, string filter, string dialect, out string context)
        {
            // transaction context
            base.Properties["TransactionDependentClone"] = Transaction.Current == null ?
                null : Transaction.Current.DependentClone(DependentCloneOption.BlockCommitUntilComplete);

            // create workflow
            WorkflowInstance wi = CreateWorkflow(Defaults.Keys.WorkflowTypeEnumerate);

            // register response event
            AsyncResponseFromLocalService ar = new AsyncResponseFromLocalService(this.WSEnumerationLocalService, wi.InstanceId);

            // fire request
            EnumerationDescriptor ed = new EnumerationDescriptor() { ResourceUri = uri, Expires = expires, Filter = filter, Dialect = dialect };
            this.WSEnumerationLocalService.RaiseRequestEvent(wi.InstanceId, ed);

            // processing
            ar.WaitForResponse(workflowTimeoutInSec);

            // result
            context = ar.Response.EnumerationDescriptor.EnumerationContext;
        }
        public void Pull(string context, int? maxTime, int? maxElements, int? maxCharacrets, out object items)
        {
            // transaction context
            base.Properties["TransactionDependentClone"] = Transaction.Current == null ?
                null : Transaction.Current.DependentClone(DependentCloneOption.BlockCommitUntilComplete);

            // create workflow
            WorkflowInstance wi = CreateWorkflow(Defaults.Keys.WorkflowTypePull);

            // register response event
            AsyncResponseFromLocalService ar = new AsyncResponseFromLocalService(this.WSEnumerationLocalService, wi.InstanceId);

            // fire request
            EnumerationDescriptor ed = new EnumerationDescriptor() { EnumerationContext = context, MaxElements = maxElements.Value };
            this.WSEnumerationLocalService.RaiseRequestEvent(wi.InstanceId, ed);

            // processing
            ar.WaitForResponse(workflowTimeoutInSec);

            // result
            items = ar.Response.Items;
        }
        public void Renew(string context, string expiresIn, out DateTime? expiresOut)
        {
            // transaction context
            base.Properties["TransactionDependentClone"] = Transaction.Current == null ?
                null : Transaction.Current.DependentClone(DependentCloneOption.BlockCommitUntilComplete);

            // create workflow
            WorkflowInstance wi = CreateWorkflow(Defaults.Keys.WorkflowTypeRenew);

            // register response event
            AsyncResponseFromLocalService ar = new AsyncResponseFromLocalService(this.WSEnumerationLocalService, wi.InstanceId);

            // fire request
            EnumerationDescriptor ed = new EnumerationDescriptor() { EnumerationContext = context, Expires = expiresIn };
            this.WSEnumerationLocalService.RaiseRequestEvent(wi.InstanceId, ed);

            // processing
            ar.WaitForResponse(workflowTimeoutInSec);

            // result
            expiresOut = DateTime.Parse(ar.Response.EnumerationDescriptor.Expires);
        }
        public void GetStatus(string context, out DateTime? expiresOut)
        {
            // transaction context
            base.Properties["TransactionDependentClone"] = Transaction.Current == null ?
                null : Transaction.Current.DependentClone(DependentCloneOption.BlockCommitUntilComplete);

            // create workflow
            WorkflowInstance wi = CreateWorkflow(Defaults.Keys.WorkflowTypeGetStatus);

            // register response event
            AsyncResponseFromLocalService ar = new AsyncResponseFromLocalService(this.WSEnumerationLocalService, wi.InstanceId);

            // fire request
            EnumerationDescriptor ed = new EnumerationDescriptor() { EnumerationContext = context };
            this.WSEnumerationLocalService.RaiseRequestEvent(wi.InstanceId, ed);

            // processing
            ar.WaitForResponse(workflowTimeoutInSec);

            // result
            expiresOut = DateTime.Parse(ar.Response.EnumerationDescriptor.Expires);
        }
        public void Release(string context)
        {
            // transaction context
            base.Properties["TransactionDependentClone"] = Transaction.Current == null ?
                null : Transaction.Current.DependentClone(DependentCloneOption.BlockCommitUntilComplete);

            // create workflow
            WorkflowInstance wi = CreateWorkflow(Defaults.Keys.WorkflowTypeRelease);

            // register response event
            AsyncResponseFromLocalService ar = new AsyncResponseFromLocalService(this.WSEnumerationLocalService, wi.InstanceId);

            // fire request
            EnumerationDescriptor ed = new EnumerationDescriptor() { EnumerationContext = context };
            this.WSEnumerationLocalService.RaiseRequestEvent(wi.InstanceId, ed);

            // processing
            ar.WaitForResponse(workflowTimeoutInSec);
        }
        #endregion

        #region helpers
        private WorkflowInstance CreateWorkflow(string workflow)
        {
            // Load and start the specified workflow
            string strWorkflowType = base.Properties[workflow] as string;
            if (strWorkflowType == null) 
            {
                strWorkflowType = base.Properties[Defaults.Keys.WorkflowType] as string;
            }
            if (strWorkflowType != null)
            {
                Dictionary<string, object> adapter = new Dictionary<string, object>();
                adapter.Add(Defaults.Keys.Config, base.Properties);
                Guid workflowInstanceId = Guid.NewGuid();
                Type workflowType = Type.GetType(strWorkflowType);
                if (workflowType != null)
                {
                    WorkflowInstance instance = this.workflowRuntime.CreateWorkflow(workflowType, adapter, workflowInstanceId);
                    instance.Start();
                    return instance;
                }
                throw new FaultException(string.Format("Can't get a workflow type '{0}'. Please, specified a workflow type in the config file.", strWorkflowType));
            }
            throw new FaultException(string.Format("The workflow '{0}' is not supported. Please, specified a workflow type in the config file.", workflow));
        }
        internal class AsyncResponseFromLocalService
        {
            AutoResetEvent _waitForResponse = new AutoResetEvent(false);
            WorkflowTerminatedEventArgs _e;
            WSEnumerationLocalService _localservice;
            ResponseEventArgs _response;
            Guid _istanceId;
            public AsyncResponseFromLocalService(WSEnumerationLocalService localservice, Guid instanceid)
            {
                _istanceId = instanceid;
                _localservice = localservice;

                // response handler
                _localservice.Response += new EventHandler<ResponseEventArgs>(localservice_FireResponse);

                // exception handler
                _localservice.WorkflowRuntime.WorkflowTerminated += new EventHandler<WorkflowTerminatedEventArgs>(OnWorkflowTerminated);
            }
            public void OnWorkflowTerminated(object sender, WorkflowTerminatedEventArgs e)
            {
                if (_istanceId == e.WorkflowInstance.InstanceId)
                {
                    this._e = e;
                    _waitForResponse.Set();
                    _localservice.WorkflowRuntime.WorkflowTerminated -= new EventHandler<WorkflowTerminatedEventArgs>(OnWorkflowTerminated);
                }
            }
            public void WaitForResponse(int secondsTimeOut)
            {
                bool retval = _waitForResponse.WaitOne(secondsTimeOut * 1000, false);
                if (_e != null)
                {
                    throw this._e.Exception;
                }
                if (retval == false)
                    throw new FaultException("The workflow timeout expired");
            }
            public ResponseEventArgs Response
            {
                get { return _response; }
            }
            public WorkflowTerminatedEventArgs WorkflowTerminatedEventArgs
            {
                get { return _e; }
            }
            public void localservice_FireResponse(object sender, ResponseEventArgs e)
            {
                if (_istanceId == e.InstanceId)
                {
                    _response = e;
                    _waitForResponse.Set();
                    _localservice.Response -= new EventHandler<ResponseEventArgs>(localservice_FireResponse);
                }
            }
        }
        #endregion
    } 
}


