//*****************************************************************************
//    Description.....Abstract class for WS-Enumeration Service Adapter
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)       
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    07/07/06    Roman Kiss     migrating to JuneCTP 
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using dc = WSEnumeration.DataContract;
#endregion

namespace DynamicUIs
{
    /// <summary>Interface to the Adapter</summary>
    public interface IWSEnumerationAdapter
    {
        void Begin      ();
        void MoveForward();
        void MoveBack   ();
        void End        ();
    }

    /// <summary>Base class of the Adapter</summary>
    /// <typeparam name="T">DataContract for resource identifier</typeparam>
    public class WSEnumerationAdapterBase<T> where T : class
    {
        private HybridDictionary _properties = null;
        public HybridDictionary Properties 
        {
            get { return _properties; } 
            set { _properties = value; } 
        }
        
        public WSEnumerationAdapterBase()
        {
        }

        //public virtual ResourceIdentifier GetResourceIdentifier<ResourceIdentifier>(MessageHeaders headers, XmlQualifiedName qname)
        //{
        //    if (headers == null || headers.Count == 0)
        //        throw new ArgumentNullException("headers");

        //    if (qname == null)
        //        throw new ArgumentNullException("qname");

        //    return headers.GetHeader<ResourceIdentifier>(qname.Name, qname.Namespace);
        //}

        //public virtual ResourceIdentifier GetResourceIdentifier<ResourceIdentifier>(MessageHeaders headers)
        //{
        //    if (headers == null || headers.Count == 0)
        //        throw new ArgumentNullException("headers");

        //    object[] rootAttribute = typeof(ResourceIdentifier).GetCustomAttributes(typeof(DataContractAttribute), false);
        //    if(rootAttribute.Length > 0)
        //    {
        //        DataContractAttribute dc = rootAttribute[0] as DataContractAttribute;
        //        return headers.GetHeader<ResourceIdentifier>(dc.Name, dc.Namespace);
        //    }
        //    return default(ResourceIdentifier);
        //}

        //public virtual T GetResourceIdentifier(MessageHeaders headers)
        //{
        //    return GetResourceIdentifier<T>(headers);
        //}

        //public virtual EndpointAddress ResourceEndpointAddress(Uri address, T resourceIdentifier)
        //{
        //    EndpointAddress resourceEndpointAddress = null;
        //    List<AddressHeader> list = new List<AddressHeader>();

        //    object[] rootAttribute = typeof(T).GetCustomAttributes(typeof(DataContractAttribute), false);
        //    if (rootAttribute.Length > 0)
        //    {
        //        DataContractAttribute dc = rootAttribute[0] as DataContractAttribute;

        //        // reference properties
        //        AddressHeader ah = AddressHeader.CreateAddressHeader(dc.Name, dc.Namespace, resourceIdentifier);
        //        list.Add(ah);
        //        AddressHeaderCollection ahc = new AddressHeaderCollection(new AddressHeaderCollection(list));

        //        // endpoint address
        //        Uri uriTo = address == null ? OperationContext.Current.IncomingMessageHeaders.To : address;
        //        resourceEndpointAddress = new EndpointAddress(uriTo, (EndpointIdentity)null, ahc);
        //    }

        //    // address and resourceIdentifier
        //    return resourceEndpointAddress;
        //}

        //public virtual EndpointAddress ResourceEndpointAddress(Uri address, IList<AddressHeader> list)
        //{
        //    AddressHeaderCollection ahc = new AddressHeaderCollection(new AddressHeaderCollection(list));

        //    // endpoint address
        //    Uri uriTo = address == null ? OperationContext.Current.IncomingMessageHeaders.To : address;
        //    EndpointAddress resourceEndpointAddress = new EndpointAddress(uriTo, (EndpointIdentity)null, ahc);

        //    // address and resourceIdentifier
        //    return resourceEndpointAddress;
        //}  
    }
}
