//*****************************************************************************
//    Description.....WS-Transfer Local Service layer for Workflow
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)         
//                        
//    Date Created:    05/05/06
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    05/05/06    Roman Kiss     Initial Revision
//    07/07/06    Roman Kiss     JuneCTP version
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
//
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Runtime.Hosting;
#endregion

namespace WSEnumeration.Adapters
{
    #region Constants
    public sealed class  Defaults 
    {
        public sealed class Values
        {
            public const int WorkflowTimeOut = 60;
        }
        public sealed class Keys
        {
            public const string Topic                   = "topic";
            public const string WorkflowTimeOut         = "WorkflowTimeOut";
            public const string Config                  = "Config";
            public const string WorkflowType            = "WorkflowType";
            public const string WorkflowTypeEnumerate   = "WorkflowTypeEnumerate";
            public const string WorkflowTypePull        = "WorkflowTypePull";
            public const string WorkflowTypeRenew       = "WorkflowTypeRenew";
            public const string WorkflowTypeGetStatus   = "WorkflowTypeGetStatus";
            public const string WorkflowTypeRelease     = "WorkflowTypeRelease";
        }
    }
    #endregion

    #region Workflow Local Service - Contract
    [ExternalDataExchange]
    public interface IWSEnumerationLocalServiceIn
    {
        // to workflow
        event EventHandler<RequestEventArgs> Request;
    }

    [ExternalDataExchange]
    public interface IWSEnumerationLocalServiceOut
    {
        // from workflow
        void RaiseResponseEvent(Guid workflowInstanceId, EnumerationDescriptor ed, object items);
    }
    #endregion

    #region Workflow Local Service - Implementation
    public class WSEnumerationLocalService : IWSEnumerationLocalServiceIn, IWSEnumerationLocalServiceOut
    {
        private WorkflowRuntime _workflowRuntime;
        private ManualWorkflowSchedulerService _scheduler;
        public WorkflowRuntime WorkflowRuntime { get { return _workflowRuntime; } }
        public ManualWorkflowSchedulerService Scheduler { get { return _scheduler; } }
        
        public WSEnumerationLocalService(WorkflowRuntime workflowRuntime)
        {
            if (workflowRuntime == null)
                throw new ArgumentNullException("workflowRuntime");

            // the workflow runtime core
            _workflowRuntime = workflowRuntime;

            // manual workflow scheduler
            _scheduler = _workflowRuntime.GetService<ManualWorkflowSchedulerService>(); 
        }

        #region IWSEnumerationLocalServiceIn Members - to workflow
        public event EventHandler<RequestEventArgs> Request;
        public void RaiseRequestEvent(Guid workflowInstanceId, EnumerationDescriptor ed)
        {         
            // Create the EventArgs for this event
            RequestEventArgs e = new RequestEventArgs(workflowInstanceId, ed);

            // Raise the event
            if (this.Request != null)
            {
                if (this.Scheduler == null)
                {
                    this.Request(null, e);
                }
                else
                {
                    this.Scheduler.RunWorkflow(workflowInstanceId);
                    this.Request(null, e);
                    this.Scheduler.RunWorkflow(workflowInstanceId);
                }
            }
        }
        #endregion

        #region IWSEnumerationLocalServiceOut Members - from workflow
        public event EventHandler<ResponseEventArgs> Response;
        public void RaiseResponseEvent(Guid workflowInstanceId, EnumerationDescriptor ed, object items)
        {
            // Create the EventArgs for this event
            ResponseEventArgs e = new ResponseEventArgs(workflowInstanceId, ed, items);

            // Raise the event
            if (this.Response != null)
            {
                this.Response(null, e);
            }
        }
        #endregion    
    }
    #endregion

    #region ExternalDataEventArgs
    [Serializable]
    public class RequestEventArgs : ExternalDataEventArgs
    {
        private EnumerationDescriptor _ed;
        private int _max;
        public RequestEventArgs(Guid InstanceId, EnumerationDescriptor ed) : base(InstanceId)
        {
            this._ed = ed;
        }
        public EnumerationDescriptor EnumerationDescriptor
        {
            get { return _ed; }
            set { _ed = value; }
        }
    }
 
    [Serializable]
    public class ResponseEventArgs : ExternalDataEventArgs
    {
        private EnumerationDescriptor _ed;
        private object _items;
        public ResponseEventArgs(Guid InstanceId, EnumerationDescriptor ed, object items)
            : base(InstanceId)
        {
            this._ed = ed;
            this._items = items;
        }
        public object Items
        {
            get { return _items; }
            set { _items = value; }
        }
        public EnumerationDescriptor EnumerationDescriptor
        {
            get { return _ed; }
            set { _ed = value; }
        }
    }
    #endregion
}


