//*****************************************************************************
//    Description.....WS-Enumeration constants 
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)         
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    07/07/06    Roman Kiss     migrating to JuneCTP 
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Xml;
using System.Xml.Serialization;
#endregion

namespace WSEnumeration
{

	/// <summary>
	/// WS-Enumeration spec constants
	/// </summary>
	public sealed class WSEnumeration
	{
        // identity
		public const string Prefix          = "wxf";
        public const string NamespaceUri    = "http://schemas.xmlsoap.org/ws/2004/09/enumeration";
        public const string ServicebusUri   = "http://www.ssafonov.net/schemas/sb/2010/12/servicebus";
        
		// WSDL operations
        // Factory
		public const string Enumerate           = "Enumerate";
		public const string EnumerateResponse   = "EnumerateResponse";
		// Operations
		public const string Pull                = "Pull";
		public const string PullResponse        = "PullResponse";
        public const string Renew               = "Renew";
        public const string RenewResponse       = "RenewResponse";
        public const string GetStatus           = "GetStatus";
        public const string GetStatusResponse   = "GetStatusResponse";
        public const string Release             = "Release";
        public const string ReleaseResponse     = "ReleaseResponse";
        public const string EnumerationEnd      = "EnumerationEnd";
        public const string Fault               = "Fault";
        public const string SourceShuttingDown  = "SourceShuttingDown";
        public const string SourceCancelling    = "SourceCancelling";
		// SOAP actions
        // Factory
        public const string EnumerateAction             = NamespaceUri + "/" + Enumerate;
		public const string EnumerateResponseAction     = NamespaceUri + "/" + EnumerateResponse;
		// Operations
		public const string PullAction                  = NamespaceUri + "/" + Pull;
		public const string PullResponseAction          = NamespaceUri + "/" + PullResponse;
        public const string RenewAction                 = NamespaceUri + "/" + Renew;
        public const string RenewResponseAction         = NamespaceUri + "/" + RenewResponse;
        public const string GetStatusAction             = NamespaceUri + "/" + GetStatus;
        public const string GetStatusResponseAction     = NamespaceUri + "/" + GetStatusResponse;
        public const string ReleaseAction               = NamespaceUri + "/" + Release;
        public const string ReleaseResponseAction       = NamespaceUri + "/" + ReleaseResponse;
        public const string EnumerationEndAction        = NamespaceUri + "/" + EnumerationEnd;
        public const string FaultAction                 = NamespaceUri + "/" + Fault;
        public const string SourceShuttingDownAction    = NamespaceUri + "/" + SourceShuttingDown;
        public const string SourceCancellingAction      = NamespaceUri + "/" + SourceCancelling;
        // Element names
        //public sealed class ElementNames
        //{
        //    public const string ResourceCreated = "ResourceCreated";
        //}
    }
}
