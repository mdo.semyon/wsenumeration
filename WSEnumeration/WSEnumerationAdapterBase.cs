//*****************************************************************************
//    Description.....Abstract class for WS-Enumeration Service Adapter
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)       
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    07/07/06    Roman Kiss     migrating to JuneCTP 
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using dc = WSEnumeration.DataContract;
#endregion

namespace WSEnumeration
{
    /// <summary>Interface to the Adapter</summary>
    public interface IWSEnumerationAdapter
    {
        void Enumerate(string uri     , string expires, string filter, string dialect, out string context);
        void Pull      (string context, int? maxTime, int? maxElements, int? maxCharacrets, out object items);
        void Renew     (string context, string expiresIn, out DateTime? expiresOut);
        void GetStatus (string context, out DateTime? expiresOut);
        void Release   (string context);
    }

    /// <summary>Base class of the Adapter</summary>
    /// <typeparam name="T">DataContract for resource identifier</typeparam>
    public class WSEnumerationAdapterBase<T> where T : class
    {
        private HybridDictionary _properties = null;
        public HybridDictionary Properties 
        {
            get { return _properties; } 
            set { _properties = value; } 
        }
        
        public WSEnumerationAdapterBase()
        {
        }
    }
}
