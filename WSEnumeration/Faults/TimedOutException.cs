﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;

namespace WSEnumeration.Faults
{
    public class TimedOutException : FaultException
    {
        public TimedOutException()
            : base("timed out",
                   FaultCode.CreateReceiverFaultCode("TimedOut", WSEnumeration.FaultAction),
                   WSEnumeration.FaultAction)
        {
        }
    }
}
