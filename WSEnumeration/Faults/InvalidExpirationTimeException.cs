﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;

namespace WSEnumeration.Faults
{
    [Serializable]
    public class InvalidExpirationTimeException : FaultException
    {
        public InvalidExpirationTimeException()
            : base("invalid expiration time",
                   FaultCode.CreateSenderFaultCode("InvalidExpirationTime", WSEnumeration.FaultAction),
                   WSEnumeration.FaultAction)
        {
        }
    }
}