﻿using System;

namespace WSEnumeration
{
    public enum FaultCodes
    {
        None = 0x0,
        CannotProcessFilter = 0x1,
        FilterDialectRequestedUnavailable = 0x2,
        FilteringNotSupported = 0x3,
        InvalidEnumerationContext = 0x4,
        InvalidExpirationTime = 0x5,
        TimedOut = 0x6,
        UnsupportedExpirationType = 0x7,
        UnableToRenew = 0x8
    }
}
