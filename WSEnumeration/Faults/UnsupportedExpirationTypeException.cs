﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;

namespace WSEnumeration.Faults
{
    public class UnsupportedExpirationTypeException : FaultException
    {
        public UnsupportedExpirationTypeException()
            : base("unsupported expiration type",
                   FaultCode.CreateSenderFaultCode("UnsupportedExpirationType", WSEnumeration.FaultAction),
                   WSEnumeration.FaultAction)
        {
        }
    }
}
