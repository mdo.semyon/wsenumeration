﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;

namespace WSEnumeration.Faults
{
    public class FilterDialectRequestedUnavailableException : FaultException
    {
        public FilterDialectRequestedUnavailableException()
            : base("filter dialect requested unavailable",
                   FaultCode.CreateSenderFaultCode("FilterDialectRequestedUnavailable", WSEnumeration.FaultAction),
                   WSEnumeration.FaultAction)
        {
        }
    }
}
