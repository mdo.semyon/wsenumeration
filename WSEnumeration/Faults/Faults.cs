//*****************************************************************************
//    Description.....WS-Eventing for WCF
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)       
//                        
//    Date Created:    06/06/06
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/06    Roman Kiss     Initial Revision
//    07/07/06    Roman Kiss     Version JuneCTP 2006
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//  
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
#endregion

namespace WSEnumeration.Faults
{
    //[XmlSchemaProvider(null, IsAny = true)]
    public class SupportedDialect //: IXmlSerializable
    {
        //private List<string> _dialects;
        //public List<string> Dialects
        //{
        //    get { return _dialects; }
        //    set { _dialects = value; }
        //}

        public SupportedDialect()
        {
            //Dialects = new List<string>();
        }
        //public SupportedDialect(List<string> dialects)
        //{
        //    Dialects = dialects;
        //}
        //public SupportedDialect(params string[] dialects) : this()
        //{
        //    foreach (string dialect in dialects)
        //    {
        //        Dialects.Add(dialect);
        //    }
        //}

        public void IsSupported()
        {
            //get
            //{
                throw new FaultException<SupportedDialect>(this, "reason", FaultCode.CreateSenderFaultCode("subcode", WSEnumeration.NamespaceUri));

            //    return false;
            //}
        }

        //#region IXmlSerializable Members
        //public System.Xml.Schema.XmlSchema GetSchema()
        //{
        //    return null;
        //}
        //public void ReadXml(XmlReader reader)
        //{
        //    reader.ReadStartElement("SupportedDialect", WSEnumeration.NamespaceUri);
        //    while (reader.NodeType != XmlNodeType.EndElement)
        //    {
        //        Dialects.Add(reader.ReadContentAsString());
        //        reader.MoveToContent();
        //    }
        //    reader.ReadEndElement();
        //}
        //public void WriteXml(XmlWriter writer)
        //{
        //    writer.WriteStartElement("wse", "SupportedDialect", WSEnumeration.NamespaceUri);
        //    if (Dialects != null && Dialects.Count > 0)
        //    {
        //        foreach (string dialect in Dialects)
        //        {
        //            writer.WriteElementString("Dialect", dialect);
        //        }
        //    }
        //    writer.WriteEndElement();
        //}
        //#endregion
    }
}
