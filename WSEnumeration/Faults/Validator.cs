﻿#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
#endregion


namespace WSEnumeration.Faults
{
    public class Validator
    {
        public Validator() { }

        public void Validate()
        {
            throw new FaultException<Validator>(this, "invalid expiration time", FaultCode.CreateSenderFaultCode("InvalidExpirationTime", WSEnumeration.NamespaceUri));
        }
    }
}
