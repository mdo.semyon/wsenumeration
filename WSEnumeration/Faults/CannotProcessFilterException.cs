﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;

namespace WSEnumeration.Faults
{
    public class CannotProcessFilterException : FaultException
    {
        public CannotProcessFilterException()
            : base("cannot process filter",
                   FaultCode.CreateSenderFaultCode("CannotProcessFilter", WSEnumeration.FaultAction),
                   WSEnumeration.FaultAction)
        {
        }
    }
}
