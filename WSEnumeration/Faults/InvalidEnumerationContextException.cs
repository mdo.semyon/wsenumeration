﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;

namespace WSEnumeration.Faults
{
    public class InvalidEnumerationContextException : FaultException
    {
        public InvalidEnumerationContextException()
            : base("Invalid enumeration context",
                   FaultCode.CreateReceiverFaultCode("InvalidEnumerationContext", WSEnumeration.FaultAction),
                   WSEnumeration.FaultAction)
        {
        }
    }
}