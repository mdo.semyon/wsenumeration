﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;

namespace WSEnumeration.Faults
{
    public class UnableToRenewException : FaultException
    {
        public UnableToRenewException()
            : base("unable to renew",
                   FaultCode.CreateReceiverFaultCode("UnableToRenew", WSEnumeration.FaultAction),
                   WSEnumeration.FaultAction)
        {
        }
    }
}
