﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;

namespace WSEnumeration.Faults
{
    public class FilteringNotSupportedException : FaultException
    {
        public FilteringNotSupportedException()
            : base("filtering not supported",
                   FaultCode.CreateSenderFaultCode("FilteringNotSupported", WSEnumeration.FaultAction),
                   WSEnumeration.FaultAction)
        {
        }
    }
}
