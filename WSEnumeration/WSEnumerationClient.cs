//*****************************************************************************
//    Description.....WS-Enumeration Client
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)        
//                        
//    Date Created:    06/06/05
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    06/06/05    Roman Kiss     Initial Revision
//    01/20/06    Roman Kiss     migrating to JanCTP (Go-Live)
//    03/03/06    Roman Kiss     migrating to FebCTP 
//    07/07/06    Roman Kiss     migrating to JuneCTP 
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
#endregion


namespace WSEnumeration
{
    /// <summary>Operation Proxy to the WSEnumeration Service</summary>
    public class WSEnumerationOperationProxy : ClientBase<IWSEnumerationOperation>, IWSEnumerationOperation
    {
        #region Constructors
        public WSEnumerationOperationProxy(){}
      
        public WSEnumerationOperationProxy(string configurationName)
            : base(configurationName){}

        public WSEnumerationOperationProxy(Binding binding, EndpointAddress address)
            : base(binding, address) { }
        #endregion

        #region IWSEnumerationOperation Members
        public PullResponse Pull(PullRequest request)
        {
            return base.Channel.Pull(request);
        }
        public RenewResponse Renew(RenewRequest request)
        {
            return base.Channel.Renew(request);
        }
        public GetStatusResponse GetStatus(GetStatusRequest request)
        {
            return base.Channel.GetStatus(request);
        }
        public ReleaseResponse Release(ReleaseRequest request)
        {
            return base.Channel.Release(request);
        }
        #endregion

        //#region API (Business) Methods
        //public object Pull()
        //{
        //   return Pull(new PullRequest()).Response;
        //}
        //#endregion
    }


    /// <summary>Factory Proxy to the WSEnumeration Service</summary>
    public class WSEnumerationFactoryProxy : ClientBase<IWSEnumerationFactory>, IWSEnumerationFactory
    {

        #region Constructors
        public WSEnumerationFactoryProxy(){}
      
        public WSEnumerationFactoryProxy(string configurationName)
            : base(configurationName){}

        public WSEnumerationFactoryProxy(Binding binding, EndpointAddress address)
            : base(binding, address) { }
        #endregion

        #region IWSEnumerationFactory
        public EnumerateResponse Enumerate(EnumerateRequest request)
        {
            return base.Channel.Enumerate(request);
        }
        #endregion

        //#region API (Business Methods)
        //public WSEnumerationOperationProxy Create(object resource)
        //{
        //    EnumerateResponse response = base.Channel.Create(new EnumerateRequest(resource));
        //    return new WSEnumerationOperationProxy( base.Endpoint.Binding, response.EndpointAddress);
        //}
        //#endregion
    }
}
