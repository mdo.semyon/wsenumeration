//*****************************************************************************
//    Description.....WCF Host Extension for Workflow Runtime
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)       
//                        
//    Date Created:    05/05/06
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    05/05/06    Roman Kiss     Initial Revision
//    11/11/06    Roman Kiss     RTM
//*****************************************************************************
//
#region References
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Channels;
//
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Runtime.Hosting;
#endregion

namespace ServiceHostExtension
{
    public class WFServiceHostExtension : IExtension<ServiceHostBase>, IDisposable
    {
        #region Properties
        private WorkflowRuntime _workflowRuntime;
        private ExternalDataExchangeService _exchangeServices;
        private string _workflowServicesConfig;
        private string _localServicesConfig;
        public WorkflowRuntime WorkflowRuntime
        {
            get { return _workflowRuntime; }
        }
        public ExternalDataExchangeService ExternalDataExchangeService
        {
            get { return _exchangeServices; }
        }
        #endregion

        #region Constructors
        public WFServiceHostExtension() : this(null, null) { }
        public WFServiceHostExtension(string workflowServicesConfig, string localServicesConfig)
        {
            _workflowServicesConfig = workflowServicesConfig;
            _localServicesConfig = localServicesConfig;
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            _workflowRuntime.Dispose();
        }
        #endregion

        #region IExtension
        void IExtension<ServiceHostBase>.Attach(ServiceHostBase owner)
        {
            // add services from config file
            if(_workflowServicesConfig == null)
                _workflowRuntime = new WorkflowRuntime();
            else
                _workflowRuntime = new WorkflowRuntime(_workflowServicesConfig);

            // not handled exception
            _workflowRuntime.ServicesExceptionNotHandled += new EventHandler<ServicesExceptionNotHandledEventArgs>(workflowRuntime_ServicesExceptionNotHandled);

            // external services
            _exchangeServices = _workflowRuntime.GetService<ExternalDataExchangeService>();
            if (_exchangeServices == null)
            {
                if (_localServicesConfig == null)
                    _exchangeServices = new ExternalDataExchangeService();
                else
                    _exchangeServices = new ExternalDataExchangeService(_localServicesConfig);

                // add service for exchange data
                _workflowRuntime.AddService(_exchangeServices);
            }

            // Start all services registered in this container
            _workflowRuntime.StartRuntime();
        }
        void IExtension<ServiceHostBase>.Detach(ServiceHostBase owner)
        {
          _workflowRuntime.StopRuntime();
         }
        #endregion

        #region Handlers
        public virtual void workflowRuntime_ServicesExceptionNotHandled(object sender, ServicesExceptionNotHandledEventArgs e)
        {
            // tbd:
        }
        #endregion
    }
}


