﻿//*****************************************************************************
//    Description.....Example(s) of the DataContract for WS-Enumeration Messages
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    24/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    24/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using wse = WSEnumeration;
#endregion

namespace WSEnumeration.Descriptors
{
    namespace FileSystem
    {
        public enum ItemType
        {
            Dir,
            File
        }

        [XmlSchemaProvider(null, IsAny = true)]
        [XmlRoot(ElementName = "FileInfo", Namespace = wse.WSEnumeration.ServicebusUri, IsNullable = true)]
        [Serializable]
        public class FileInfoDescriptor : IXmlSerializable
        {
            #region data
            public string Name              { get; set; }
            public Int32 Size               { get; set; }
            public string Extension         { get; set; }
            public string Root              { get; set; }
            public DateTime LastWriteTime   { get; set; }
            public ItemType Type            { get; set; }
            #endregion

            #region .ctor
            public FileInfoDescriptor()
            {
                Name = default(string);
            }
            public FileInfoDescriptor(XmlReader reader)
            {
                this.ReadXml(reader);
            }
            #endregion

            #region IXmlSerializable Members
            public System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }
            public void ReadXml(XmlReader reader)
            {
                int s = 0;

                if (reader.HasAttributes)
                    while (reader.MoveToNextAttribute())
                    {
                        if (reader.LocalName == "Name")
                            Name = reader.Value;
                        if (reader.LocalName == "Size")
                        {
                            if (Int32.TryParse(reader.Value, out s))
                                Size = s;
                        }
                        if (reader.LocalName == "Extension")
                            Extension = reader.Value;
                        if (reader.LocalName == "Root")
                            Root = reader.Value;
                        if (reader.LocalName == "LastWriteTime")
                            LastWriteTime = DateTime.Parse(reader.Value);
                        if (reader.LocalName == "Type")
                        {
                            try
                            {
                                Type = (ItemType)Enum.Parse(typeof(ItemType), reader.Value);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                reader.MoveToElement();

                if (reader.IsEmptyElement)
                {
                    reader.Skip();
                    return;
                }

                reader.ReadStartElement("FileInfo", wse.WSEnumeration.ServicebusUri);
                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    if (reader.IsStartElement("Name", wse.WSEnumeration.ServicebusUri))
                        Name = reader.ReadElementContentAsString();
                    else if (reader.IsStartElement("Size", wse.WSEnumeration.ServicebusUri))
                    {
                        if (Int32.TryParse(reader.ReadElementContentAsString(), out s))
                            Size = s;
                    }
                    else if (reader.IsStartElement("Extension", wse.WSEnumeration.ServicebusUri))
                        Extension = reader.ReadElementContentAsString();
                    else if (reader.IsStartElement("Root", wse.WSEnumeration.ServicebusUri))
                        Root = reader.ReadElementContentAsString();
                    else if (reader.IsStartElement("LastWriteTime", wse.WSEnumeration.ServicebusUri))
                        LastWriteTime = reader.ReadElementContentAsDateTime();
                    else if (reader.IsStartElement("Type", wse.WSEnumeration.ServicebusUri))
                    {
                        try
                        {
                            Type = (ItemType)Enum.Parse(typeof(ItemType), reader.ReadElementContentAsString());
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        reader.MoveToContent();
                        break;
                    }
                    reader.MoveToContent();
                }
                reader.ReadEndElement();
            }
            public void WriteXml(XmlWriter writer)
            {
                writer.WriteStartElement("FileInfo", wse.WSEnumeration.ServicebusUri);

                if (!String.IsNullOrEmpty(Name))
                    writer.WriteAttributeString("Name", Name);
                    
                writer.WriteAttributeString("Size", Size.ToString());

                if (!String.IsNullOrEmpty(Extension))
                    writer.WriteAttributeString("Extension", Extension);
                if (!String.IsNullOrEmpty(Root))
                    writer.WriteAttributeString("Root", Root);

                writer.WriteAttributeString("LastWriteTime", LastWriteTime.ToString());

                if (Type != null)
                    writer.WriteAttributeString("Type", Enum.GetName(typeof(ItemType), Type));

                writer.WriteEndElement();
            }
            #endregion
        }
    }
}
