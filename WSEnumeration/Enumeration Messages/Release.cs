﻿//*****************************************************************************
//    Description.....WS-Enumeration Release Request/Response Message
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
using dc = WSEnumeration.DataContract;
#endregion

namespace WSEnumeration
{
    #region ReleaseRequest
    [MessageContract(IsWrapped = false)]
    public class ReleaseRequest
    {
        #region DataMembers
        dc.Release _data = null;
        #endregion

        #region Properties
        [MessageBodyMember]public dc.Release Release { get { return _data; } set { _data = value; } }
        #endregion

        #region Constructors
        public ReleaseRequest() {}
        public ReleaseRequest(dc.Release data) { Release = data; }
        #endregion
    }
    #endregion

    #region ReleaseResponse
    [MessageContract(IsWrapped = false)]
    public class ReleaseResponse
    {
        #region Constructors
        public ReleaseResponse() {}
        #endregion
    }
    #endregion

}
