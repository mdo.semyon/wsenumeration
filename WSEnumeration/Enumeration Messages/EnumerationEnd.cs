﻿//*****************************************************************************
//    Description.....WS-Enumeration EnumerationEnd Request/Response Message
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
using dc = WSEnumeration.DataContract;
#endregion

namespace WSEnumeration
{
    #region EnumerationEndRequest
    [MessageContract(IsWrapped = false)]
    public class EnumerationEndRequest
    {
        #region DataMembers
        dc.EnumerationEnd _data = null;
        #endregion

        #region Properties
        [MessageBodyMember]public dc.EnumerationEnd EnumerationEnd { get { return _data; } set { _data = value; } }
        #endregion

        #region Constructors
        public EnumerationEndRequest() {}
        public EnumerationEndRequest(dc.EnumerationEnd data) { EnumerationEnd = data; }
        #endregion
    }
    #endregion
}
