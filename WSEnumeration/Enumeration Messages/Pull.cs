//*****************************************************************************
//    Description.....WS-Enumeration Pull Request/Response Message
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    24/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    24/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
using dc = WSEnumeration.DataContract;
#endregion

namespace WSEnumeration
{
    #region PullRequest
    [MessageContract(IsWrapped = false)]
    public class PullRequest
    {
        #region DataMembers
        dc.Pull _data = null;
        #endregion

        #region Properties
        [MessageBodyMember(Name = WSEnumeration.Pull, Namespace = WSEnumeration.NamespaceUri, Order = 0)]
        public dc.Pull Pull { get { return _data; } set { _data = value; } }
        #endregion

        #region Constructors
        public PullRequest() {}
        public PullRequest(dc.Pull data) { Pull = data; }
        #endregion
    }
    #endregion

    #region PullResponse
    [MessageContract(IsWrapped = false)]
    public class PullResponse
    {
        #region DataMembers
        dc.PullResponse _data = null;
        #endregion

        #region Properties
        [MessageBodyMember(Name = WSEnumeration.PullResponse, Namespace = WSEnumeration.NamespaceUri, Order = 0 )]
        public dc.PullResponse Response { get { return _data; } set { _data = value; } }
        #endregion

        #region Constructors
        public PullResponse() {}
        public PullResponse(dc.PullResponse data) { Response = data; }
        #endregion
    }
    #endregion
}