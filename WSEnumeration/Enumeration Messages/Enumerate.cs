//*****************************************************************************
//    Description.....WS-Enumeration Enumerate Request/Response Message
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
using dc = WSEnumeration.DataContract;
#endregion

namespace WSEnumeration
{
    #region EnumerateRequest
    [MessageContract(IsWrapped=false)]
    public class EnumerateRequest
    {
        #region DataMembers
        string          _resourceUri;
        dc.Enumerate    _enumerate;
        #endregion

        #region Properties
        [MessageHeader    ]public string        ResourceUri { get { return _resourceUri;    } set { _resourceUri    = value; } }
        [MessageBodyMember]public dc.Enumerate  Enumerate   { get { return _enumerate;      } set { _enumerate      = value; } }
        #endregion

        #region Constructors
        public EnumerateRequest() : this((dc.Enumerate)null) {}
        public EnumerateRequest(dc.Enumerate enumerate) { Enumerate = enumerate; }
        #endregion 
    }
    #endregion

    #region EnumerateResponse
    [MessageContract(IsWrapped = false)]
    public class EnumerateResponse 
    {
        #region DataMembers
        dc.EnumerateResponse _response  = null;
        dc.EnumerationEnd    _end       = null;
        #endregion

        #region Properties
        [MessageBodyMember(Name = WSEnumeration.EnumerateResponse, Namespace = WSEnumeration.NamespaceUri, Order = 0)]
        public dc.EnumerateResponse Response { get { return _response; } set { _response = value; } }

        [MessageBodyMember(Name = WSEnumeration.EnumerationEnd, Namespace = WSEnumeration.NamespaceUri, Order = 1)]
        public dc.EnumerationEnd End
        {
            get
            {
                if (_response != null)
                    return null;
                else
                    return _end;
            }
            set { _end = value; }
        }
        #endregion

        #region Constructors
        public EnumerateResponse() {}
        public EnumerateResponse(dc.EnumerateResponse response) { Response = response; }
        public EnumerateResponse(dc.EnumerationEnd end) { End = end; }
        #endregion
    }
    #endregion
}

