﻿//*****************************************************************************
//    Description.....WS-Enumeration GetStatus Request/Response Message
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
using dc = WSEnumeration.DataContract;
#endregion

namespace WSEnumeration
{
    #region GetStatusRequest
    [MessageContract(IsWrapped = false)]
    public class GetStatusRequest
    {
        #region DataMembers
        dc.GetStatus _data = null;
        #endregion

        #region Properties
        [MessageBodyMember]public dc.GetStatus GetStatus { get { return _data; } set { _data = value; } }
        #endregion

        #region Constructors
        public GetStatusRequest() {}
        public GetStatusRequest(dc.GetStatus data) { GetStatus = data; }
        #endregion
    }
    #endregion

    #region GetStatusResponse
    [MessageContract(IsWrapped = false)]
    public class GetStatusResponse
    {
        #region DataMembers
        dc.GetStatusResponse _data = null;
        #endregion

        #region Properties
        [MessageBodyMember(Name = WSEnumeration.GetStatusResponse, Namespace = WSEnumeration.NamespaceUri, Order = 0)]
        public dc.GetStatusResponse Response { get { return _data; } set { _data = value; } }
        #endregion

        #region Constructors
        public GetStatusResponse() {}
        public GetStatusResponse(dc.GetStatusResponse data) { Response = data; }
        #endregion
    }
    #endregion

}
