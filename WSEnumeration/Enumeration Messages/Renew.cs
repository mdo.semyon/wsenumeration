﻿//*****************************************************************************
//    Description.....WS-Enumeration Renew Request/Response Message
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    26/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    26/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.Runtime.Serialization;
using dc = WSEnumeration.DataContract;
#endregion

namespace WSEnumeration
{
    #region RenewRequest
    [MessageContract(IsWrapped = false)]
    public class RenewRequest
    {
        #region DataMembers
        dc.Renew _data = null;
        #endregion

        #region Properties
        [MessageBodyMember]public dc.Renew Renew { get { return _data; } set { _data = value; } }
        #endregion

        #region Constructors
        public RenewRequest() {}
        public RenewRequest(dc.Renew data) { Renew = data; }
        #endregion
    }
    #endregion

    #region RenewResponse
    [MessageContract(IsWrapped = false)]
    public class RenewResponse
    {
        #region DataMembers
        dc.RenewResponse _data = null;
        #endregion

        #region Properties
        [MessageBodyMember(Name = WSEnumeration.RenewResponse, Namespace = WSEnumeration.NamespaceUri, Order = 0)]
        public dc.RenewResponse Response { get { return _data; } set { _data = value; } }
        #endregion

        #region Constructors
        public RenewResponse() {}
        public RenewResponse(dc.RenewResponse data) { Response = data; }
        #endregion
    }
    #endregion
}
