﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace WSEnumerationWorkflow.FileSystem
{
    partial class WorkflowRelease
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding1 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding2 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding3 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            this.WSEnumerationReleaseResponse = new System.Workflow.Activities.CallExternalMethodActivity();
            this.ReleaseExecutor = new System.Workflow.Activities.CodeActivity();
            this.WSEnumerationReleaseRequest = new System.Workflow.Activities.HandleExternalEventActivity();
            // 
            // WSEnumerationReleaseResponse
            // 
            this.WSEnumerationReleaseResponse.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceOut);
            this.WSEnumerationReleaseResponse.MethodName = "RaiseResponseEvent";
            this.WSEnumerationReleaseResponse.Name = "WSEnumerationReleaseResponse";
            activitybind1.Name = "WorkflowRelease";
            activitybind1.Path = "EnumerationDescriptor";
            workflowparameterbinding1.ParameterName = "ed";
            workflowparameterbinding1.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            activitybind2.Name = "WorkflowRelease";
            activitybind2.Path = "WorkflowInstanceID";
            workflowparameterbinding2.ParameterName = "workflowInstanceId";
            workflowparameterbinding2.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            this.WSEnumerationReleaseResponse.ParameterBindings.Add(workflowparameterbinding1);
            this.WSEnumerationReleaseResponse.ParameterBindings.Add(workflowparameterbinding2);
            this.WSEnumerationReleaseResponse.MethodInvoking += new System.EventHandler(this.WSEnumerationReleaseResponse_Invoking);
            // 
            // ReleaseExecutor
            // 
            this.ReleaseExecutor.Name = "ReleaseExecutor";
            this.ReleaseExecutor.ExecuteCode += new System.EventHandler(this.ProcessRelease_ExecuteCode);
            // 
            // WSEnumerationReleaseRequest
            // 
            this.WSEnumerationReleaseRequest.EventName = "Request";
            this.WSEnumerationReleaseRequest.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceIn);
            this.WSEnumerationReleaseRequest.Name = "WSEnumerationReleaseRequest";
            activitybind3.Name = "WorkflowRelease";
            activitybind3.Path = "RequestEvent";
            workflowparameterbinding3.ParameterName = "e";
            workflowparameterbinding3.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            this.WSEnumerationReleaseRequest.ParameterBindings.Add(workflowparameterbinding3);
            this.WSEnumerationReleaseRequest.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.WSEnumerationReleaseRequest_Invoked);
            // 
            // WorkflowRelease
            // 
            this.Activities.Add(this.WSEnumerationReleaseRequest);
            this.Activities.Add(this.ReleaseExecutor);
            this.Activities.Add(this.WSEnumerationReleaseResponse);
            this.Name = "WorkflowRelease";
            this.CanModifyActivities = false;

        }

        #endregion

        private CallExternalMethodActivity WSEnumerationReleaseResponse;
        private CodeActivity ReleaseExecutor;
        private HandleExternalEventActivity WSEnumerationReleaseRequest;








    }
}
