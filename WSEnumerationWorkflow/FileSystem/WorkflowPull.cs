//*****************************************************************************
//    Description.....FileSystem Storage for Workflow
//                                
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    27/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    27/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Transactions;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

using System.Text.RegularExpressions;

//
using System.IO;
using System.Linq;
using LinqToFileSystem;
using WSEnumeration;
using WSEnumeration.Descriptors.FileSystem;
using WSEnumeration.Adapters;
using f = WSEnumeration.Faults;
#endregion

namespace WSEnumerationWorkflow.FileSystem
{
    public sealed partial class WorkflowPull : SequentialWorkflowActivity
    {
        #region Constructor
        public WorkflowPull()
        {
            InitializeComponent();
            Console.WriteLine("\nWorkflowPull has been initiated");
        }
        #endregion

        #region Private Members
        string _storagePath = default(string);
        Hashtable _storage = null;
        Hashtable _expires = null;
        #endregion

        #region Mandatory Property - Configuration
        public static DependencyProperty ConfigProperty = System.Workflow.ComponentModel.DependencyProperty.Register("Config", typeof(HybridDictionary), typeof(WorkflowPull));

        [Description("This is the description which appears in the Property Browser")]
        [Category("Config properties")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public HybridDictionary Config
        {
            get
            {
                return ((HybridDictionary)(base.GetValue(WorkflowPull.ConfigProperty)));
            }
            set
            {
                base.SetValue(WorkflowPull.ConfigProperty, value);
            }
        }
        #endregion

        #region Local Service
        private RequestEventArgs _getRequestEvent = default(RequestEventArgs);
        public RequestEventArgs RequestEvent
        {
            get { return _getRequestEvent; }
            set { _getRequestEvent = value; }
        }
        #endregion

        #region Workflow's handlers
        private void WorkflowPull_Initialized(object sender, EventArgs e)
        {
            _storage = AppDomain.CurrentDomain.GetData("Contexts") as Hashtable;
            if (_storage == null)
                throw new f.SourceCancellingException();

            _expires = AppDomain.CurrentDomain.GetData("Expires") as Hashtable;
            if (_expires == null)
                throw new f.SourceCancellingException();

            _storagePath = Config["StoragePath"] as string;
            if (_storagePath == null)
                throw new Exception("Missing storagePath to the storage folder");
        }

        private void WorkflowPull_Completed(object sender, EventArgs e)
        {
            // todo:
        }
        #endregion

        #region WSEnumeration members
        public static DependencyProperty EnumerationDescriptorProperty =
            System.Workflow.ComponentModel.DependencyProperty.Register("EnumerationDescriptor",
                                                                       typeof(EnumerationDescriptor),
                                                                       typeof(WorkflowPull));
        public static DependencyProperty WorkflowInstanceIDProperty =
            System.Workflow.ComponentModel.DependencyProperty.Register("WorkflowInstanceID",
                                                                       typeof(Guid),
                                                                       typeof(WorkflowPull));
        public static DependencyProperty ItemsProperty =
            System.Workflow.ComponentModel.DependencyProperty.Register("Items",
                                                                        typeof(object),
                                                                        typeof(WorkflowPull));

        [Description("This is the description which appears in the Property Browser")]
        [Category("This is the category which will be displayed in the Property Browser")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public EnumerationDescriptor EnumerationDescriptor
        {
            get
            {
                return ((EnumerationDescriptor)(base.GetValue(WorkflowPull.EnumerationDescriptorProperty)));
            }
            set
            {
                base.SetValue(WorkflowPull.EnumerationDescriptorProperty, value);
            }
        }

        [Description("This is the description which appears in the Property Browser")]
        [Category("This is the category which will be displayed in the Property Browser")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Guid WorkflowInstanceID
        {
            get
            {
                return ((Guid)(base.GetValue(WorkflowPull.WorkflowInstanceIDProperty)));
            }
            set
            {
                base.SetValue(WorkflowPull.WorkflowInstanceIDProperty, value);
            }
        }

        [Description("This is the description which appears in the Property Browser")]
        [Category("This is the category which will be displayed in the Property Browser")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public object Items
        {
            get
            {
                return base.GetValue(WorkflowPull.ItemsProperty);
            }
            set
            {
                base.SetValue(WorkflowPull.ItemsProperty, value);
            }
        }
        #endregion

        #region Pull
        private void WSEnumerationPullRequest_Invoked(object sender, ExternalDataEventArgs e)
        {
            WorkflowInstanceID = e.InstanceId;
            EnumerationDescriptor = RequestEvent.EnumerationDescriptor;

            Console.WriteLine("WSEnumerationPullRequest_Invoked, wf={0}, rd={1}", WorkflowInstanceID, EnumerationDescriptor.EnumerationContext);
        }

        /// <summary>
        /// If the enumeration context is not valid, either because it has been replaced in the response 
        /// to another Pull request, or because it has completed (EndOfSequence has been returned in a Pull 
        /// response), or because it has been Released, or because it has expired, or because the data source 
        /// has had to invalidate the context, then the data source SHOULD fail the request, and MAY generate 
        /// the following fault:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProcessPull_ExecuteCode(object sender, EventArgs e)
        {
            Marker marker;
            ItemDescriptorCollection<FileInfoDescriptor> lst = null;
            FileSystemInfo fsi = null;
            FileInfoDescriptor dscr = null;

            Console.WriteLine("ProcessPull_ExecuteCode, wf={0}, rd={1}", WorkflowInstanceID, EnumerationDescriptor.EnumerationContext);
            if (Items == null)
                Items = new ItemDescriptorCollection<FileInfoDescriptor>();

            lst = Items as ItemDescriptorCollection<FileInfoDescriptor>;
            // action
            lock (_storage)
            lock (_expires)
            {
                if (!_storage.Contains(EnumerationDescriptor.EnumerationContext) || (DateTime)_expires[EnumerationDescriptor.EnumerationContext] <= DateTime.Now)
                    throw new f.InvalidEnumerationContextException();

                marker = (Marker)_storage[EnumerationDescriptor.EnumerationContext];

                if (marker.Count == 0)
                    throw new f.InvalidEnumerationContextException();

                for (int i = 0; i < EnumerationDescriptor.MaxElements; i++)
                {
                    if (marker.Dirs.MoveNext())
                    {
                        fsi = marker.Dirs.Current as FileSystemInfo;

                        dscr = new FileInfoDescriptor();
                        dscr.Root = (fsi as DirectoryInfo).AncestorsAndSelf().Reverse().Aggregate("", (ac, d) => ac + ( Regex.IsMatch(d.Name, "[A-Za-z][:]") ? d.Name : d.Name + "/"), ac => ac.Remove(0, _storagePath.Length).Trim('/'));
                        dscr.Type = ItemType.Dir;
                        dscr.Name = fsi.Name;
                        dscr.Extension = fsi.Extension;
                        dscr.LastWriteTime = fsi.LastWriteTime;
                        lst.Add(dscr);

                        marker.Count--;

                        if (marker.Count == 0)
                            lst.EndOfSequence = true;
                    }
                    else if (marker.Files.MoveNext())
                    {
                        fsi = marker.Files.Current as FileSystemInfo;

                        dscr = new FileInfoDescriptor();
                        dscr.Type = ItemType.File;
                        dscr.Name = fsi.Name;
                        dscr.Size = Convert.ToInt32((fsi as FileInfo).Length);
                        dscr.Extension = fsi.Extension;
                        dscr.LastWriteTime = fsi.LastWriteTime;
                        lst.Add(dscr);

                        marker.Count--;

                        if (marker.Count == 0)
                            lst.EndOfSequence = true;
                    }
                    else
                        break;
                }
                return;
            }
            throw new f.InvalidEnumerationContextException();
        }

        private void WSEnumerationPullResponse_Invoking(object sender, EventArgs e)
        {
            Console.WriteLine("WSEnumerationPullResponse_Invoking, wf={0}, rd={1}", WorkflowInstanceID, EnumerationDescriptor.EnumerationContext);
        }
        #endregion
    }

}


