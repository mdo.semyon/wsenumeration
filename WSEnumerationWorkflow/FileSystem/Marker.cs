﻿#region
using System;
using System.Collections;
#endregion


namespace WSEnumerationWorkflow.FileSystem
{
	internal class Marker
	{
        public IEnumerator Dirs { get; set; }
        public IEnumerator Files   { get; set; }
        public int         Count   { get; set; }
	}
}
