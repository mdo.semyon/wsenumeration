using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;


namespace WSEnumerationWorkflow.FileSystem
{
    public sealed partial class WorkflowEnumerate
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference1 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference2 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference3 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference4 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding1 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding2 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding3 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            this.ProcessEnumeration = new System.Workflow.Activities.CodeActivity();
            this.FilterDialectRequestedUnavailableException = new System.Workflow.ComponentModel.ThrowActivity();
            this.FilteringNotSupportedException = new System.Workflow.ComponentModel.ThrowActivity();
            this.InvalidExpirationTimeException = new System.Workflow.ComponentModel.ThrowActivity();
            this.UnsupportedExpirationTypeException = new System.Workflow.ComponentModel.ThrowActivity();
            this.ParamsAreOK = new System.Workflow.Activities.IfElseBranchActivity();
            this.FilterDialectRequestedUnavailable = new System.Workflow.Activities.IfElseBranchActivity();
            this.FilteringNotSupported = new System.Workflow.Activities.IfElseBranchActivity();
            this.InvalidExpirationTime = new System.Workflow.Activities.IfElseBranchActivity();
            this.UnsupportedExpirationType = new System.Workflow.Activities.IfElseBranchActivity();
            this.WSEnumerationEnumerateResponse = new System.Workflow.Activities.CallExternalMethodActivity();
            this.RequestParams = new System.Workflow.Activities.IfElseActivity();
            this.WSEnumerationEnumerateRequest = new System.Workflow.Activities.HandleExternalEventActivity();
            // 
            // ProcessEnumeration
            // 
            this.ProcessEnumeration.Name = "ProcessEnumeration";
            this.ProcessEnumeration.ExecuteCode += new System.EventHandler(this.ProcessEnumeration_ExecuteCode);
            // 
            // FilterDialectRequestedUnavailableException
            // 
            this.FilterDialectRequestedUnavailableException.FaultType = typeof(WSEnumeration.Faults.FilterDialectRequestedUnavailableException);
            this.FilterDialectRequestedUnavailableException.Name = "FilterDialectRequestedUnavailableException";
            // 
            // FilteringNotSupportedException
            // 
            this.FilteringNotSupportedException.FaultType = typeof(WSEnumeration.Faults.FilteringNotSupportedException);
            this.FilteringNotSupportedException.Name = "FilteringNotSupportedException";
            // 
            // InvalidExpirationTimeException
            // 
            this.InvalidExpirationTimeException.FaultType = typeof(WSEnumeration.Faults.InvalidExpirationTimeException);
            this.InvalidExpirationTimeException.Name = "InvalidExpirationTimeException";
            // 
            // UnsupportedExpirationTypeException
            // 
            this.UnsupportedExpirationTypeException.FaultType = typeof(WSEnumeration.Faults.UnsupportedExpirationTypeException);
            this.UnsupportedExpirationTypeException.Name = "UnsupportedExpirationTypeException";
            // 
            // ParamsAreOK
            // 
            this.ParamsAreOK.Activities.Add(this.ProcessEnumeration);
            this.ParamsAreOK.Name = "ParamsAreOK";
            // 
            // FilterDialectRequestedUnavailable
            // 
            this.FilterDialectRequestedUnavailable.Activities.Add(this.FilterDialectRequestedUnavailableException);
            ruleconditionreference1.ConditionName = "c4";
            this.FilterDialectRequestedUnavailable.Condition = ruleconditionreference1;
            this.FilterDialectRequestedUnavailable.Name = "FilterDialectRequestedUnavailable";
            // 
            // FilteringNotSupported
            // 
            this.FilteringNotSupported.Activities.Add(this.FilteringNotSupportedException);
            ruleconditionreference2.ConditionName = "c3";
            this.FilteringNotSupported.Condition = ruleconditionreference2;
            this.FilteringNotSupported.Name = "FilteringNotSupported";
            // 
            // InvalidExpirationTime
            // 
            this.InvalidExpirationTime.Activities.Add(this.InvalidExpirationTimeException);
            ruleconditionreference3.ConditionName = "c2";
            this.InvalidExpirationTime.Condition = ruleconditionreference3;
            this.InvalidExpirationTime.Name = "InvalidExpirationTime";
            // 
            // UnsupportedExpirationType
            // 
            this.UnsupportedExpirationType.Activities.Add(this.UnsupportedExpirationTypeException);
            ruleconditionreference4.ConditionName = "c1";
            this.UnsupportedExpirationType.Condition = ruleconditionreference4;
            this.UnsupportedExpirationType.Name = "UnsupportedExpirationType";
            // 
            // WSEnumerationEnumerateResponse
            // 
            this.WSEnumerationEnumerateResponse.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceOut);
            this.WSEnumerationEnumerateResponse.MethodName = "RaiseResponseEvent";
            this.WSEnumerationEnumerateResponse.Name = "WSEnumerationEnumerateResponse";
            activitybind1.Name = "WorkflowEnumerate";
            activitybind1.Path = "WorkflowInstanceID";
            workflowparameterbinding1.ParameterName = "workflowInstanceId";
            workflowparameterbinding1.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            activitybind2.Name = "WorkflowEnumerate";
            activitybind2.Path = "RequestEvent.EnumerationDescriptor";
            workflowparameterbinding2.ParameterName = "ed";
            workflowparameterbinding2.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            this.WSEnumerationEnumerateResponse.ParameterBindings.Add(workflowparameterbinding1);
            this.WSEnumerationEnumerateResponse.ParameterBindings.Add(workflowparameterbinding2);
            this.WSEnumerationEnumerateResponse.MethodInvoking += new System.EventHandler(this.WSEnumerationEnumerateResponse_Invoking);
            // 
            // RequestParams
            // 
            this.RequestParams.Activities.Add(this.UnsupportedExpirationType);
            this.RequestParams.Activities.Add(this.InvalidExpirationTime);
            this.RequestParams.Activities.Add(this.FilteringNotSupported);
            this.RequestParams.Activities.Add(this.FilterDialectRequestedUnavailable);
            this.RequestParams.Activities.Add(this.ParamsAreOK);
            this.RequestParams.Name = "RequestParams";
            // 
            // WSEnumerationEnumerateRequest
            // 
            this.WSEnumerationEnumerateRequest.EventName = "Request";
            this.WSEnumerationEnumerateRequest.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceIn);
            this.WSEnumerationEnumerateRequest.Name = "WSEnumerationEnumerateRequest";
            activitybind3.Name = "WorkflowEnumerate";
            activitybind3.Path = "RequestEvent";
            workflowparameterbinding3.ParameterName = "e";
            workflowparameterbinding3.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            this.WSEnumerationEnumerateRequest.ParameterBindings.Add(workflowparameterbinding3);
            this.WSEnumerationEnumerateRequest.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.WSEnumerationEnumerateRequest_Invoked);
            // 
            // WorkflowEnumerate
            // 
            this.Activities.Add(this.WSEnumerationEnumerateRequest);
            this.Activities.Add(this.RequestParams);
            this.Activities.Add(this.WSEnumerationEnumerateResponse);
            this.Description = "Workflow for testing WS-Transfer Create Resource";
            this.Name = "WorkflowEnumerate";
            this.Initialized += new System.EventHandler(this.WorkflowEnumerate_Initialized);
            this.Completed += new System.EventHandler(this.WorkflowEnumerate_Completed);
            this.CanModifyActivities = false;

        }

        #endregion

        private ThrowActivity InvalidExpirationTimeException;
        private IfElseBranchActivity UnsupportedExpirationType;
        private IfElseBranchActivity InvalidExpirationTime;
        private IfElseActivity RequestParams;
        private IfElseBranchActivity FilteringNotSupported;
        private IfElseBranchActivity ParamsAreOK;
        private IfElseBranchActivity FilterDialectRequestedUnavailable;
        private ThrowActivity FilterDialectRequestedUnavailableException;
        private ThrowActivity FilteringNotSupportedException;
        private ThrowActivity UnsupportedExpirationTypeException;
        private CallExternalMethodActivity WSEnumerationEnumerateResponse;
        private CodeActivity ProcessEnumeration;
        private HandleExternalEventActivity WSEnumerationEnumerateRequest;





























































































































    }
}
