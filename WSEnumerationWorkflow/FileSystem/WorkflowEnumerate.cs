//*****************************************************************************
//    Description.....FileSystem Storage for Workflow
//                                
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    27/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    27/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region
using System;
using System.Text.RegularExpressions;

using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

using System.ComponentModel;
using System.ComponentModel.Design;

using System.Drawing;
using System.Transactions;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
//
using System.IO;
using System.Linq;

using LinqToFileSystem;
using WSEnumeration;
using WSEnumeration.Adapters;
using f = WSEnumeration.Faults;
#endregion

namespace WSEnumerationWorkflow.FileSystem
{
    public sealed partial class WorkflowEnumerate : SequentialWorkflowActivity
    {
        #region Constructor
        public WorkflowEnumerate()
        {
            InitializeComponent();
            Console.WriteLine("\nWorkflowEnumerate has been initiated");
        }
        #endregion

        #region Private Members
        string _storagePath = default(string);
        string[] _dialects;
        DateTime _expiresTime;
        Hashtable _storage = null;
        Hashtable _expires = null;
        DirectoryInfo _dir = null;
        #endregion

        #region Mandatory Property - Configuration
        public static DependencyProperty ConfigProperty = System.Workflow.ComponentModel.DependencyProperty.Register("Config", typeof(HybridDictionary), typeof(WorkflowEnumerate));
        [Description("This is the description which appears in the Property Browser")]
        [Category("Config properties")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public HybridDictionary Config
        {
            get { return ((HybridDictionary)(base.GetValue(WorkflowEnumerate.ConfigProperty))); }
            set { base.SetValue(WorkflowEnumerate.ConfigProperty, value); }
        }
        #endregion

        #region Local Service
        private RequestEventArgs _requestEvent = default(RequestEventArgs);
        public RequestEventArgs RequestEvent
        {
            get { return _requestEvent; }
            set { _requestEvent = value; }
        }
        #endregion

        #region Workflow's handlers
        private void WorkflowEnumerate_Initialized(object sender, EventArgs e)
        {
            _storage = AppDomain.CurrentDomain.GetData("Contexts") as Hashtable;
            if (_storage == null)
            {
                lock (AppDomain.CurrentDomain)
                {
                    _storage = AppDomain.CurrentDomain.GetData("Contexts") as Hashtable;
                    if (_storage == null)
                    {
                        _storage = Hashtable.Synchronized(new Hashtable());
                        AppDomain.CurrentDomain.SetData("Contexts", _storage);
                    }
                }
            }
            _expires = AppDomain.CurrentDomain.GetData("Expires") as Hashtable;
            if (_expires == null)
            {
                lock (AppDomain.CurrentDomain)
                {
                    _expires = AppDomain.CurrentDomain.GetData("Expires") as Hashtable;
                    if (_expires == null)
                    {
                        _expires = Hashtable.Synchronized(new Hashtable());
                        AppDomain.CurrentDomain.SetData("Expires", _expires);
                    }
                }
            }

            _storagePath = Config["StoragePath"] as string;
            if (_storagePath == null)
                throw new Exception("Missing storagePath to the storage folder");

            //_dir = new DirectoryInfo(_storagePath);
            //if (!_dir.Exists)
            //    _dir.Create();
        }

        private void WorkflowEnumerate_Completed(object sender, EventArgs e)
        {
            // todo:
        }
        #endregion

        #region WSEnumeration members
        public static DependencyProperty EnumerationDescriptorProperty =
            System.Workflow.ComponentModel.DependencyProperty.Register("EnumerationDescriptor",
                                                                       typeof(EnumerationDescriptor),
                                                                       typeof(WorkflowEnumerate));
        public static DependencyProperty WorkflowInstanceIDProperty =
            System.Workflow.ComponentModel.DependencyProperty.Register("WorkflowInstanceID",
                                                                       typeof(Guid),
                                                                       typeof(WorkflowEnumerate));

        [Description("This is the description which appears in the Property Browser")]
        [Category("This is the category which will be displayed in the Property Browser")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public EnumerationDescriptor EnumerationDescriptor
        {
            get { return ((EnumerationDescriptor)(base.GetValue(WorkflowEnumerate.EnumerationDescriptorProperty))); }
            set { base.SetValue(WorkflowEnumerate.EnumerationDescriptorProperty, value); }
        }

        [Description("This is the description which appears in the Property Browser")]
        [Category("This is the category which will be displayed in the Property Browser")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Guid WorkflowInstanceID
        {
            get { return ((Guid)(base.GetValue(WorkflowEnumerate.WorkflowInstanceIDProperty))); }
            set { base.SetValue(WorkflowEnumerate.WorkflowInstanceIDProperty, value); }
        }
        #endregion

        #region Create
        private void WSEnumerationEnumerateRequest_Invoked(object sender, ExternalDataEventArgs e)
        {
            WorkflowInstanceID = e.InstanceId;
            EnumerationDescriptor = RequestEvent.EnumerationDescriptor;

            Console.WriteLine("WSEnumerationEnumerateRequest_Invoked, wf={0}, ctx={1}", WorkflowInstanceID, EnumerationDescriptor.EnumerationContext);
        }
        private void ProcessEnumeration_ExecuteCode(object sender, EventArgs e)
        {
            Marker marker = new Marker();

            //if(_dialects.Contains("XPath"))
            //{
                _dir = new DirectoryInfo(_storagePath + ( String.IsNullOrEmpty(EnumerationDescriptor.Filter) ? "" : "/" + EnumerationDescriptor.Filter ));

                if (!_dir.Exists)
                    throw new DirectoryNotFoundException(_storagePath + EnumerationDescriptor.Filter);

                FileSystemTreeAdapter adapter = new FileSystemTreeAdapter(_dir);

                var dirs = adapter.Children();
                var files = from f in _dir.Files() select f;

                marker.Dirs = dirs.GetEnumerator();
                marker.Files = files.GetEnumerator();
                marker.Count = dirs.Count() + files.Count();
            //}

            if (marker.Files == null)
                throw new f.CannotProcessFilterException();

            lock (_storage)
                lock (_expires)
                {
                    EnumerationDescriptor.EnumerationContext = Guid.NewGuid().ToString();

                    _storage.Add(EnumerationDescriptor.EnumerationContext, marker);
                    _expires.Add(EnumerationDescriptor.EnumerationContext, _expiresTime);
                }
            Console.WriteLine("MemoryStorageEnumerate_ExecuteCode, wf={0}, ctx={1}", WorkflowInstanceID, EnumerationDescriptor.EnumerationContext);
        }
        private void WSEnumerationEnumerateResponse_Invoking(object sender, EventArgs e)
        {
            Console.WriteLine("WSEnumerationEnumerateResponse_Invoking, wf={0}, ctx={1}", WorkflowInstanceID, EnumerationDescriptor.EnumerationContext);
        }
        #endregion

        #region Validators
        internal bool IsValidExpirationTime
        {
            get
            {
                int i_dT = 0;
                string str_dT = default(string);

                if (DateTime.TryParse(EnumerationDescriptor.Expires, out _expiresTime))
                {
                    if (_expiresTime <= DateTime.Now)
                        return false;
                }
                else
                {
                    if (EnumerationDescriptor.Expires.Contains("D"))
                    {
                        str_dT = EnumerationDescriptor.Expires.Replace("D", String.Empty);
                        if (Int32.TryParse(str_dT, out i_dT) && i_dT > 0)
                        {
                            _expiresTime = DateTime.Now.AddDays(i_dT);
                            return true;
                        }
                    }
                    else if (EnumerationDescriptor.Expires.Contains("H"))
                    {
                        str_dT = EnumerationDescriptor.Expires.Replace("H", String.Empty);
                        if (Int32.TryParse(str_dT, out i_dT) && i_dT > 0)
                        {
                            _expiresTime = DateTime.Now.AddHours(i_dT);
                            return true;
                        }
                    }
                    else if (EnumerationDescriptor.Expires.Contains("M"))
                    {
                        str_dT = EnumerationDescriptor.Expires.Replace("M", String.Empty);
                        if (Int32.TryParse(str_dT, out i_dT) && i_dT > 0)
                        {
                            _expiresTime = DateTime.Now.AddMinutes(i_dT);
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        internal bool IsSupportedExpirationType
        {
            get
            {
                Match matches;
                string[] supportedTypes = Config["ExpirationType"].ToString().Replace(" ", String.Empty).Split('|');

                if (DateTime.TryParse(EnumerationDescriptor.Expires, out _expiresTime) && supportedTypes.Contains("T"))
                    return true;

                foreach (var item in supportedTypes)
                {
                    matches = Regex.Match(EnumerationDescriptor.Expires, @"\d{1,}" + item, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                    if (matches.Success)
                        return true;
                }

                return false;
            }
        }
        internal bool IsFilteringSupported
        {
            get
            {
                bool res;

                if (String.IsNullOrEmpty(EnumerationDescriptor.Filter))
                    return true;

                if (Boolean.TryParse(Config["Filtering"].ToString(), out res))
                    return res;

                return false;
            }
        }
        internal bool IsFilterDialectRequestedAvailable
        {
            get
            {
                if (String.IsNullOrEmpty(EnumerationDescriptor.Dialect))
                    return true;

                _dialects = Config["FilterDialect"].ToString().Replace(" ", String.Empty).Split('|');

                if (_dialects.Contains(EnumerationDescriptor.Dialect))
                    return true;

                return false;
            }
        }
        #endregion
    }
}
