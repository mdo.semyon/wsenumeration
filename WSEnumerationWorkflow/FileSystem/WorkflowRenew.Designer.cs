﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace WSEnumerationWorkflow.FileSystem
{
    partial class WorkflowRenew
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding1 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding2 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding3 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            this.WSEnumerationRenewResponse = new System.Workflow.Activities.CallExternalMethodActivity();
            this.RenewExecutor = new System.Workflow.Activities.CodeActivity();
            this.WSEnumerationRenewRequest = new System.Workflow.Activities.HandleExternalEventActivity();
            // 
            // WSEnumerationRenewResponse
            // 
            this.WSEnumerationRenewResponse.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceOut);
            this.WSEnumerationRenewResponse.MethodName = "RaiseResponseEvent";
            this.WSEnumerationRenewResponse.Name = "WSEnumerationRenewResponse";
            activitybind1.Name = "WorkflowRenew";
            activitybind1.Path = "EnumerationDescriptor";
            workflowparameterbinding1.ParameterName = "ed";
            workflowparameterbinding1.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            activitybind2.Name = "WorkflowRenew";
            activitybind2.Path = "WorkflowInstanceID";
            workflowparameterbinding2.ParameterName = "workflowInstanceId";
            workflowparameterbinding2.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            this.WSEnumerationRenewResponse.ParameterBindings.Add(workflowparameterbinding1);
            this.WSEnumerationRenewResponse.ParameterBindings.Add(workflowparameterbinding2);
            this.WSEnumerationRenewResponse.MethodInvoking += new System.EventHandler(this.WSEnumerationRenewResponse_Invoking);
            // 
            // RenewExecutor
            // 
            this.RenewExecutor.Name = "RenewExecutor";
            this.RenewExecutor.ExecuteCode += new System.EventHandler(this.ProcessRenew_ExecuteCode);
            // 
            // WSEnumerationRenewRequest
            // 
            this.WSEnumerationRenewRequest.EventName = "Request";
            this.WSEnumerationRenewRequest.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceIn);
            this.WSEnumerationRenewRequest.Name = "WSEnumerationRenewRequest";
            activitybind3.Name = "WorkflowRenew";
            activitybind3.Path = "RequestEvent";
            workflowparameterbinding3.ParameterName = "e";
            workflowparameterbinding3.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            this.WSEnumerationRenewRequest.ParameterBindings.Add(workflowparameterbinding3);
            this.WSEnumerationRenewRequest.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.WSEnumerationRenewRequest_Invoked);
            // 
            // WorkflowRenew
            // 
            this.Activities.Add(this.WSEnumerationRenewRequest);
            this.Activities.Add(this.RenewExecutor);
            this.Activities.Add(this.WSEnumerationRenewResponse);
            this.Name = "WorkflowRenew";
            this.CanModifyActivities = false;

        }

        #endregion

        private CallExternalMethodActivity WSEnumerationRenewResponse;
        private CodeActivity RenewExecutor;
        private HandleExternalEventActivity WSEnumerationRenewRequest;











    }
}
