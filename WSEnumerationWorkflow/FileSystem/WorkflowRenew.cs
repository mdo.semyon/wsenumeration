﻿//*****************************************************************************
//    Description.....FileSystem Storage for Workflow
//                                
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    27/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    27/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Transactions;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
//
using System.IO;
using System.Linq;
using LinqToFileSystem;
using WSEnumeration;
using WSEnumeration.Adapters;
using f = WSEnumeration.Faults;
#endregion

namespace WSEnumerationWorkflow.FileSystem
{
    public sealed partial class WorkflowRenew : SequentialWorkflowActivity
    {
        #region Constructor
        public WorkflowRenew()
        {
            InitializeComponent();
            Console.WriteLine("\nWorkflowRenew has been initiated");
        }
        #endregion

        #region Private Members
        Hashtable _storage = null;
        Hashtable _expires = null;
        #endregion

        #region Mandatory Property - Configuration
        public static DependencyProperty ConfigProperty = System.Workflow.ComponentModel.DependencyProperty.Register("Config", typeof(HybridDictionary), typeof(WorkflowRenew));
        [Description("This is the description which appears in the Property Browser")]
        [Category("Config properties")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public HybridDictionary Config
        {
            get
            {
                return ((HybridDictionary)(base.GetValue(WorkflowRenew.ConfigProperty)));
            }
            set
            {
                base.SetValue(WorkflowRenew.ConfigProperty, value);
            }
        }
        #endregion

        #region Local Service
        private RequestEventArgs _requestEvent = default(RequestEventArgs);
        public RequestEventArgs RequestEvent
        {
            get { return _requestEvent; }
            set { _requestEvent = value; }
        }
        #endregion

        #region Workflow's handlers
        private void WorkflowRenew_Initialized(object sender, EventArgs e)
        {
            _storage = AppDomain.CurrentDomain.GetData("Contexts") as Hashtable;
            if (_storage == null)
                throw new f.SourceCancellingException();

            _expires = AppDomain.CurrentDomain.GetData("Expires") as Hashtable;
            if (_expires == null)
                throw new f.SourceCancellingException();
        }

        private void WorkflowRenew_Completed(object sender, EventArgs e)
        {
            // todo:
        }
        #endregion

        #region WSEnumeration members
        public static DependencyProperty EnumerationDescriptorProperty =
            System.Workflow.ComponentModel.DependencyProperty.Register("EnumerationDescriptor",
                                                                       typeof(EnumerationDescriptor),
                                                                       typeof(WorkflowRenew));
        public static DependencyProperty WorkflowInstanceIDProperty =
            System.Workflow.ComponentModel.DependencyProperty.Register("WorkflowInstanceID",
                                                                       typeof(Guid),
                                                                       typeof(WorkflowRenew));

        [Description("This is the description which appears in the Property Browser")]
        [Category("This is the category which will be displayed in the Property Browser")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public EnumerationDescriptor EnumerationDescriptor
        {
            get
            {
                return ((EnumerationDescriptor)(base.GetValue(WorkflowRenew.EnumerationDescriptorProperty)));
            }
            set
            {
                base.SetValue(WorkflowRenew.EnumerationDescriptorProperty, value);
            }
        }

        [Description("This is the description which appears in the Property Browser")]
        [Category("This is the category which will be displayed in the Property Browser")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Guid WorkflowInstanceID
        {
            get
            {
                return ((Guid)(base.GetValue(WorkflowRenew.WorkflowInstanceIDProperty)));
            }
            set
            {
                base.SetValue(WorkflowRenew.WorkflowInstanceIDProperty, value);
            }
        }
        #endregion

        #region Renew
        private void WSEnumerationRenewRequest_Invoked(object sender, ExternalDataEventArgs e)
        {
            WorkflowInstanceID = e.InstanceId;
            EnumerationDescriptor = RequestEvent.EnumerationDescriptor;

            Console.WriteLine("WSEnumerationRenewRequest_Invoked, wf={0}, ctx={1}", WorkflowInstanceID, EnumerationDescriptor.EnumerationContext);
        }

        private void ProcessRenew_ExecuteCode(object sender, EventArgs e)
        {
           //todo:
        }

        private void WSEnumerationRenewResponse_Invoking(object sender, EventArgs e)
        {
            Console.WriteLine("WSEnumerationRenewResponse_Invoking, wf={0}, ctx={1}", WorkflowInstanceID, EnumerationDescriptor.EnumerationContext);
        }
        #endregion
    }

}
