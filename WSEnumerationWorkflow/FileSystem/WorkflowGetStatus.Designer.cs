﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace WSEnumerationWorkflow.FileSystem
{
    partial class WorkflowGetStatus
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding1 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding2 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding3 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            this.WSEnumerationGetStatusResponse = new System.Workflow.Activities.CallExternalMethodActivity();
            this.GetStatusExecutor = new System.Workflow.Activities.CodeActivity();
            this.WSEnumerationGetStatusRequest = new System.Workflow.Activities.HandleExternalEventActivity();
            // 
            // WSEnumerationGetStatusResponse
            // 
            this.WSEnumerationGetStatusResponse.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceOut);
            this.WSEnumerationGetStatusResponse.MethodName = "RaiseResponseEvent";
            this.WSEnumerationGetStatusResponse.Name = "WSEnumerationGetStatusResponse";
            activitybind1.Name = "WorkflowGetStatus";
            activitybind1.Path = "EnumerationDescriptor";
            workflowparameterbinding1.ParameterName = "ed";
            workflowparameterbinding1.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            activitybind2.Name = "WorkflowGetStatus";
            activitybind2.Path = "WorkflowInstanceID";
            workflowparameterbinding2.ParameterName = "workflowInstanceId";
            workflowparameterbinding2.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            this.WSEnumerationGetStatusResponse.ParameterBindings.Add(workflowparameterbinding1);
            this.WSEnumerationGetStatusResponse.ParameterBindings.Add(workflowparameterbinding2);
            this.WSEnumerationGetStatusResponse.MethodInvoking += new System.EventHandler(this.WSEnumerationGetStatusResponse_Invoking);
            // 
            // GetStatusExecutor
            // 
            this.GetStatusExecutor.Name = "GetStatusExecutor";
            this.GetStatusExecutor.ExecuteCode += new System.EventHandler(this.ProcessGetStatus_ExecuteCode);
            // 
            // WSEnumerationGetStatusRequest
            // 
            this.WSEnumerationGetStatusRequest.EventName = "Request";
            this.WSEnumerationGetStatusRequest.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceIn);
            this.WSEnumerationGetStatusRequest.Name = "WSEnumerationGetStatusRequest";
            activitybind3.Name = "WorkflowGetStatus";
            activitybind3.Path = "RequestEvent";
            workflowparameterbinding3.ParameterName = "e";
            workflowparameterbinding3.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            this.WSEnumerationGetStatusRequest.ParameterBindings.Add(workflowparameterbinding3);
            this.WSEnumerationGetStatusRequest.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.WSEnumerationGetStatusRequest_Invoked);
            // 
            // WorkflowGetStatus
            // 
            this.Activities.Add(this.WSEnumerationGetStatusRequest);
            this.Activities.Add(this.GetStatusExecutor);
            this.Activities.Add(this.WSEnumerationGetStatusResponse);
            this.Name = "WorkflowGetStatus";
            this.CanModifyActivities = false;

        }

        #endregion

        private CallExternalMethodActivity WSEnumerationGetStatusResponse;
        private CodeActivity GetStatusExecutor;
        private HandleExternalEventActivity WSEnumerationGetStatusRequest;








    }
}
