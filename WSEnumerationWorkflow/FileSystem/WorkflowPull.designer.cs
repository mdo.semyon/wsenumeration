using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace WSEnumerationWorkflow.FileSystem
{
    public sealed partial class WorkflowPull
    {


        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding1 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding2 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding3 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind4 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding4 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            this.WSEnumerationPullResponse = new System.Workflow.Activities.CallExternalMethodActivity();
            this.PullExecuter = new System.Workflow.Activities.CodeActivity();
            this.WSEnumerationPullRequest = new System.Workflow.Activities.HandleExternalEventActivity();
            // 
            // WSEnumerationPullResponse
            // 
            this.WSEnumerationPullResponse.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceOut);
            this.WSEnumerationPullResponse.MethodName = "RaiseResponseEvent";
            this.WSEnumerationPullResponse.Name = "WSEnumerationPullResponse";
            activitybind1.Name = "WorkflowPull";
            activitybind1.Path = "RequestEvent.EnumerationDescriptor";
            workflowparameterbinding1.ParameterName = "ed";
            workflowparameterbinding1.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            activitybind2.Name = "WorkflowPull";
            activitybind2.Path = "WorkflowInstanceID";
            workflowparameterbinding2.ParameterName = "workflowInstanceId";
            workflowparameterbinding2.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            activitybind3.Name = "WorkflowPull";
            activitybind3.Path = "Items";
            workflowparameterbinding3.ParameterName = "items";
            workflowparameterbinding3.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            this.WSEnumerationPullResponse.ParameterBindings.Add(workflowparameterbinding1);
            this.WSEnumerationPullResponse.ParameterBindings.Add(workflowparameterbinding2);
            this.WSEnumerationPullResponse.ParameterBindings.Add(workflowparameterbinding3);
            this.WSEnumerationPullResponse.MethodInvoking += new System.EventHandler(this.WSEnumerationPullResponse_Invoking);
            // 
            // PullExecuter
            // 
            this.PullExecuter.Name = "PullExecuter";
            this.PullExecuter.ExecuteCode += new System.EventHandler(this.ProcessPull_ExecuteCode);
            // 
            // WSEnumerationPullRequest
            // 
            this.WSEnumerationPullRequest.EventName = "Request";
            this.WSEnumerationPullRequest.InterfaceType = typeof(WSEnumeration.Adapters.IWSEnumerationLocalServiceIn);
            this.WSEnumerationPullRequest.Name = "WSEnumerationPullRequest";
            activitybind4.Name = "WorkflowPull";
            activitybind4.Path = "RequestEvent";
            workflowparameterbinding4.ParameterName = "e";
            workflowparameterbinding4.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind4)));
            this.WSEnumerationPullRequest.ParameterBindings.Add(workflowparameterbinding4);
            this.WSEnumerationPullRequest.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.WSEnumerationPullRequest_Invoked);
            // 
            // WorkflowPull
            // 
            this.Activities.Add(this.WSEnumerationPullRequest);
            this.Activities.Add(this.PullExecuter);
            this.Activities.Add(this.WSEnumerationPullResponse);
            this.Name = "WorkflowPull";
            this.Initialized += new System.EventHandler(this.WorkflowPull_Initialized);
            this.Completed += new System.EventHandler(this.WorkflowPull_Completed);
            this.CanModifyActivities = false;

        }

        private CallExternalMethodActivity WSEnumerationPullResponse;
        private CodeActivity PullExecuter;
        private HandleExternalEventActivity WSEnumerationPullRequest;

    }
}
