﻿//*****************************************************************************
//    Description.....Example(s) of the DataContract for WS-Enumeration Messages
//                                                 
//    Author..........Sam Safonov, samsaf@gmail.com
//                        
//    Date Created:    24/12/10
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    24/12/10    Sam Safonov     Initial Revision
//*****************************************************************************
//
#region References
using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
#endregion

namespace WSEnumerationWorkflow.FileSystem
{
    public class ItemsDescriptor : IXmlSerializable
    {
        #region data
        public IList<ItemDescriptor> Items { get; set; }
        #endregion

        #region .ctor
        public ItemsDescriptor() 
        {
            Items = new List<ItemDescriptor>();
        }
        #endregion

        #region IXmlSerializable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("Items", "http://www.rkiss.net/schemas/sb/2005/06/servicebus");

            while (reader.NodeType != XmlNodeType.EndElement)
            {
                ItemDescriptor obj = new ItemDescriptor(reader);
                Items.Add(obj);

                reader.MoveToContent();
            }
            reader.ReadEndElement();
        }
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Items", "http://www.rkiss.net/schemas/sb/2005/06/servicebus");

            foreach(var item in Items)
                item.WriteXml(writer);
            
            writer.WriteEndElement();
        }
        #endregion
    }

    public class ItemDescriptor : IXmlSerializable
    {
        #region data
        public string Name { get; set; }
        #endregion

        #region .ctor
        public ItemDescriptor() 
        { 
            Name = default(string);
        }
        public ItemDescriptor(XmlReader reader)
        {
            this.ReadXml(reader);
        }
        #endregion

        #region IXmlSerializable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            if (reader.HasAttributes)
                while (reader.MoveToNextAttribute())
                    if (reader.LocalName == "Name")
                        Name = reader.Value;
                reader.MoveToElement();

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement("FileInfo", "http://www.rkiss.net/schemas/sb/2005/06/servicebus");
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                if (reader.IsStartElement("Name", "http://www.rkiss.net/schemas/sb/2005/06/servicebus"))
                    Name = reader.ReadElementContentAsString();
                else
                    break;

                reader.MoveToContent();
            }
            reader.ReadEndElement();
        }
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("FileInfo", "http://www.rkiss.net/schemas/sb/2005/06/servicebus");
            writer.WriteAttributeString("Name", Name);
            writer.WriteEndElement();
        }
        #endregion
    
    }
}
