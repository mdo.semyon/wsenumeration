//*****************************************************************************
//    Description.....Client Application process
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)         
//                        
//    Date Created:    05/05/06
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    05/05/06    Roman Kiss     Initial Revision
//    07/07/06    Roman Kiss     JuneCTP version
//*****************************************************************************
//
#region References
using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Messaging;
using System.Transactions;
//
using WSEnumeration;
using WSEnumeration.Adapters;
using dc = WSEnumeration.DataContract;
using f = WSEnumeration.Faults;
#endregion

namespace ClientApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            EnumerateResponse res = null;
            EnumerateResponse res1 = null;

            try
            {
                for (int ii = 0; ii < 1000; ii++)
                {

                    using (ChannelFactory<IWSEnumeration> enumChannel = new ChannelFactory<IWSEnumeration>("PublicStorage"))
                    {
                        enumChannel.Open();
                        IWSEnumeration enumerator = enumChannel.CreateChannel();

                        using (TransactionScope txscope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromSeconds(35)))
                        {
                            try
                            {
                                if(ii == 0) res = enumerator.Enumerate(new EnumerateRequest() { ResourceUri = "s1", Enumerate = new dc.Enumerate() { Expires = "1M" } });
                                var items = enumerator.Pull(new PullRequest(new dc.Pull() { EnumerationContext = res.Response.EnumerationContext, MaxElements = 5 }));
                            }
                            catch (FaultException ex)
                            {
                                Console.WriteLine("faulted!!!!!!!!!!!");
                            }


                            try
                            {
                                if (ii == 0) res1 = enumerator.Enumerate(new EnumerateRequest() { ResourceUri = "s2", Enumerate = new dc.Enumerate() { Expires = "2M" } });
                                var items1 = enumerator.Pull(new PullRequest(new dc.Pull() { EnumerationContext = res1.Response.EnumerationContext, MaxElements = 2 }));
                            }
                            catch (FaultException ex)
                            {
                                Console.WriteLine("faulted!!!!!!!!!!!");
                            }

                            //// -----< testing CreateResponse >--------------- 
                            //ArrayList myResource = new ArrayList(2);
                            //myResource.Add("Roman");
                            //myResource.Add(DateTime.Now);
                            //CreateRequest createRequest = new CreateRequest(myResource);
                            //CreateResponse createResponse = storage.Create(createRequest);
                            //Util.ShowMe(createResponse.EndpointAddress10);

                            //Console.WriteLine("\nDone, Press any key to test again"); //Console.ReadLine();

                            //// redirect channel to operation
                            //storageChannel.Endpoint.Address = createResponse.EndpointAddress;

                            //// get identifier from factory response
                            //AddressHeader ahdr = createResponse.EndpointAddress.Headers.FindHeader(ResourceDescriptor.DataContractName, ResourceDescriptor.DataContractNamespace);
                            //ResourceDescriptor identifier = ahdr.GetValue<ResourceDescriptor>();

                            //// get Resource  
                            //WSEnumeration.GetRequest getRequest = new WSEnumeration.GetRequest(identifier);
                            //WSEnumeration.GetResponse getResponse = storage.Get(getRequest);
                            //Util.ShowMe(getResponse);

                            //// put resource
                            //Console.WriteLine("\n Press any key to 'Put Resource'"); //Console.ReadLine();
                            //object myNewResource = Guid.NewGuid();
                            //Util.ShowMe(myNewResource);
                            //storage.Put(new PutRequest(myNewResource, identifier));

                            //// get Resource  
                            //getResponse = storage.Get(new WSEnumeration.GetRequest(identifier));
                            //Util.ShowMe(getResponse);

                            
                            txscope.Complete();
                        }
                        Console.WriteLine("\nDone, Press any key to test again"); Console.ReadLine();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.ReadLine();
        }     
    }

    public sealed class Util
    {
        private Util() { }
        public static void ShowMe(object resource)
        {
            Console.WriteLine();
            if (resource != null)
            {
                XmlTextWriter xtw = new XmlTextWriter(Console.Out);
                xtw.Formatting = Formatting.Indented;
                XmlSerializer xs = new XmlSerializer(resource.GetType());
                xs.Serialize(xtw, resource);
            }
            Console.WriteLine();
        }
    }
}





/*
      using (TransactionScope txscope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromSeconds(35)))
       {
         MessageQueue mq = new MessageQueue(@".\private$\wcfwf");
         System.Messaging.Message outMsg = new System.Messaging.Message(DateTime.Now, new BinaryMessageFormatter());
         mq.Send(outMsg, "=== Client ==========", MessageQueueTransactionType.Automatic);
         
         txscope.Complete();
                   
       }
 */

