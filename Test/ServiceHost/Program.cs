//*****************************************************************************
//    Description.....Service Host process
//                                
//    Author..........Roman Kiss, rkiss@pathcom.com
//    Copyright � 2005 ATZ Consulting Inc. (see included license.rtf file)         
//                        
//    Date Created:    05/05/06
//
//    Date        Modified By     Description
//-----------------------------------------------------------------------------
//    05/05/06    Roman Kiss     Initial Revision
//    07/07/06    Roman Kiss     JuneCTP version
//*****************************************************************************
//
#region
using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Management;
using WSEnumeration;
//using WSEnumeration.Services;
//
using ServiceHostExtension;
#endregion

namespace ServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (System.ServiceModel.ServiceHost host = new System.ServiceModel.ServiceHost(typeof(WSEnumerationService)))
                {
                    // Service extension for WF 
                    WFServiceHostExtension extension = new WFServiceHostExtension("WorkflowRuntimeConfig", "LocalServicesConfig");

                    // Add the Extension to the ServiceHost collection
                    host.Extensions.Add(extension);

                    host.Open();

                    Console.WriteLine("Press any key to stop server...");
                    Console.ReadLine();
                    host.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
            }
        }
    }
}
