﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.ServiceModel;
using WSEnumeration;

namespace TestApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (ChannelFactory<IWSEnumeration> enumChannel = new ChannelFactory<IWSEnumeration>("PublicStorage"))
            {
                var form = new DirListView();
                enumChannel.Open();
                IWSEnumeration enumerator = enumChannel.CreateChannel();
                Presenter presenter = new Presenter(enumerator, form);

                Application.Run(form);
            }
        }
    }
}
