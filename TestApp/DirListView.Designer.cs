﻿namespace TestApp
{
    partial class DirListView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._dgv = new System.Windows.Forms.DataGridView();
            this._btnUp = new System.Windows.Forms.Button();
            this._tb = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this._dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // _dgv
            // 
            this._dgv.AllowUserToAddRows = false;
            this._dgv.AllowUserToDeleteRows = false;
            this._dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgv.Location = new System.Drawing.Point(12, 38);
            this._dgv.Name = "_dgv";
            this._dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._dgv.Size = new System.Drawing.Size(636, 292);
            this._dgv.TabIndex = 0;
            this._dgv.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this._dgv_CellMouseDoubleClick);
            // 
            // _btnUp
            // 
            this._btnUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnUp.Image = global::TestApp.Properties.Resources.up;
            this._btnUp.Location = new System.Drawing.Point(615, 10);
            this._btnUp.Name = "_btnUp";
            this._btnUp.Size = new System.Drawing.Size(33, 23);
            this._btnUp.TabIndex = 1;
            this._btnUp.UseVisualStyleBackColor = true;
            this._btnUp.Click += new System.EventHandler(this._btnUp_Click);
            // 
            // _tb
            // 
            this._tb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._tb.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this._tb.Location = new System.Drawing.Point(12, 12);
            this._tb.Name = "_tb";
            this._tb.ReadOnly = true;
            this._tb.Size = new System.Drawing.Size(597, 20);
            this._tb.TabIndex = 2;
            // 
            // DirListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 342);
            this.Controls.Add(this._tb);
            this.Controls.Add(this._btnUp);
            this.Controls.Add(this._dgv);
            this.Name = "DirListView";
            this.Text = "Directory List";
            ((System.ComponentModel.ISupportInitialize)(this._dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _dgv;
        private System.Windows.Forms.Button _btnUp;
        private System.Windows.Forms.TextBox _tb;
    }
}

