﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using System.Transactions;

//
using WSEnumeration;
using WSEnumeration.Adapters;
using WSEnumeration.Descriptors;
using dc = WSEnumeration.DataContract;
using f = WSEnumeration.Faults;
using fs = WSEnumeration.Descriptors.FileSystem;

namespace TestApp
{
    public class Presenter
    {
        #region data
        IWSEnumeration _enumerator;
        DirListView _view;

        IList<fs.FileInfoDescriptor> _lst;
        IList<DataDisplayObject> _data;

        string _xpath = default(string);
        #endregion

        #region ctor
        public Presenter(IWSEnumeration theEnumerator, DirListView theView)
        {
            Init(theEnumerator, theView);
        }
        #endregion


        #region private
        void Init(IWSEnumeration theEnumerator, DirListView theView)
        {
            _enumerator = theEnumerator;
            _view = theView;

            _data = GetDirs();
            _view.SetPresenter(this);
            _view.Init();

            LLUpdateDesired();
        }
        IList<DataDisplayObject> GetDirs()
        {
            IList<DataDisplayObject> lst = new List<DataDisplayObject>();

            using (TransactionScope txscope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromSeconds(35)))
            {
                try
                {
                    var ctx = _enumerator.Enumerate(new EnumerateRequest() { Enumerate = new dc.Enumerate() { Expires = "1M" } });
                    var res = _enumerator.Pull(new PullRequest(new dc.Pull() { EnumerationContext = ctx.Response.EnumerationContext, MaxElements = 100 }));

                    //ItemsDescriptors should implement IEnumerable
                    ItemDescriptorCollection<fs.FileInfoDescriptor> dscrs = res.Response.Items as ItemDescriptorCollection<fs.FileInfoDescriptor>;

                    if (dscrs != null)
                        foreach (var item in dscrs)
                        {
                            DataDisplayObject data = new DataDisplayObject(item);
                            lst.Add(data);
                        }
                }
                catch (FaultException ex)
                {
                    return null;
                }
                txscope.Complete();
            }

            return lst;
        }
        #endregion

        #region low level
        void LLUpdateDesired()
        {
            _view.ResetData(_xpath, _data);
        }
        #endregion

        #region public
        public void DrillDownDesired(int iRow) 
        {
            IList<DataDisplayObject> lst = new List<DataDisplayObject>();

            if (_data[iRow].Inner.Type == WSEnumeration.Descriptors.FileSystem.ItemType.File)
                return;

            using (TransactionScope txscope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromSeconds(35)))
            {
                try
                {
                    dc.Enumerate enumRequestBody = new dc.Enumerate();
                    enumRequestBody.Expires = "1M";
                    enumRequestBody.Dialect = "XPath";
                    _xpath = enumRequestBody.Filter = _data[iRow].Inner.Root;

                    var ctx = _enumerator.Enumerate(new EnumerateRequest(enumRequestBody));

                    dc.Pull pullRequestBody = new dc.Pull();
                    pullRequestBody.MaxElements = 100;
                    pullRequestBody.EnumerationContext = ctx.Response.EnumerationContext;

                    var res = _enumerator.Pull(new PullRequest(pullRequestBody));

                    ItemDescriptorCollection<fs.FileInfoDescriptor> dscrs = res.Response.Items as ItemDescriptorCollection<fs.FileInfoDescriptor>;

                    if (dscrs != null)
                    {
                        foreach (var item in dscrs)
                        {
                            DataDisplayObject data = new DataDisplayObject(item);
                            lst.Add(data);
                        }
                    }

                }
                catch (FaultException ex)
                {

                }
                finally
                {
                    txscope.Complete();
                }
            }

            //_lst = GetDirs(key);

           // LLUpdateDesired();

            _data = lst;
            LLUpdateDesired();
        }
        public void UpDesired() 
        {

            if (String.IsNullOrEmpty(_xpath))
                return;

            IList<DataDisplayObject> lst = new List<DataDisplayObject>();

            using (TransactionScope txscope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromSeconds(35)))
            {
                try
                {
                    dc.Enumerate enumRequestBody = new dc.Enumerate();
                    enumRequestBody.Expires = "1M";
                    enumRequestBody.Dialect = "XPath";

                    string[] parts = _xpath.Split('/');
                    string filter = String.Empty;

                    for (int i = 0; i < parts.Length; i++)
                    {
                        if (i == parts.Length - 1)
                            continue;

                        filter = filter + "/" + parts[i];
                    }
                    _xpath = enumRequestBody.Filter = filter.Trim('/');

                    var ctx = _enumerator.Enumerate(new EnumerateRequest(enumRequestBody));

                    dc.Pull pullRequestBody = new dc.Pull();
                    pullRequestBody.MaxElements = 100;
                    pullRequestBody.EnumerationContext = ctx.Response.EnumerationContext;

                    var res = _enumerator.Pull(new PullRequest(pullRequestBody));

                    ItemDescriptorCollection<fs.FileInfoDescriptor> dscrs = res.Response.Items as ItemDescriptorCollection<fs.FileInfoDescriptor>;

                    if (dscrs != null)
                    {
                        foreach (var item in dscrs)
                        {
                            DataDisplayObject data = new DataDisplayObject(item);
                            lst.Add(data);
                        }
                    }

                }
                catch (FaultException ex)
                {

                }
                finally
                {
                    txscope.Complete();
                }
            }

            _data = lst;
            LLUpdateDesired();
        }
        #endregion
    }
}
