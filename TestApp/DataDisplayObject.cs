﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ws = WSEnumeration;
using fs = WSEnumeration.Descriptors.FileSystem;


namespace TestApp
{
    public class DataDisplayObject
    {
        fs.FileInfoDescriptor _inner;

        public System.Drawing.Image IsFile { get { return _inner.Type == fs.ItemType.File ? Properties.Resources.blank.ToBitmap() : Properties.Resources.folder.ToBitmap(); } }
        public string Name { get { return _inner.Name; } }
        public string Size 
        { 
            get 
            {
                double k, m;
                string size = String.Empty;

                if(_inner.Size > 0 && _inner.Size < 1024)
                    size = _inner.Size.ToString() + " bytes";
                else if (_inner.Size >= 1024)
                {
                    k = _inner.Size / 1024f;

                    if (k > 1024)
                    {
                        m = k / 1024f;
                        size = Convert.ToInt32(m).ToString() + " MB";
                    }
                    else
                    {
                        size = Convert.ToInt32(k).ToString() + " KB";
                    }
                }

                return size; 
            } 
        }
        public string Extension { get { return _inner.Extension; } }
        public DateTime LastWriteTime { get { return _inner.LastWriteTime; } }

        public DataDisplayObject(fs.FileInfoDescriptor aDescriptor)
        {
            _inner = aDescriptor;
        }
        public fs.FileInfoDescriptor Inner { get { return _inner; } }
    }
}
