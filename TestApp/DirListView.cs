﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace TestApp
{
    public partial class DirListView : Form
    {
        #region data
        Presenter _presenter;

        DataGridViewImageColumn _dgvtcIsFile;
        DataGridViewTextBoxColumn _dgvtcName;
        DataGridViewTextBoxColumn _dgvtcSize;
        DataGridViewTextBoxColumn _dgvtcLastWriteTime;
        #endregion

        public DirListView()
        {
            InitializeComponent();
        }

        #region private
        void SetupColumns()
        {
            this._dgv.AutoGenerateColumns = false;

            _dgvtcIsFile = new DataGridViewImageColumn();
            _dgvtcName = new DataGridViewTextBoxColumn();
            _dgvtcSize = new DataGridViewTextBoxColumn();
            _dgvtcLastWriteTime = new DataGridViewTextBoxColumn();

            _dgvtcIsFile.DataPropertyName = "IsFile";
            _dgvtcName.DataPropertyName = "Name";
            _dgvtcSize.DataPropertyName = "Size";
            _dgvtcLastWriteTime.DataPropertyName = "LastWriteTime";


            _dgvtcIsFile.HeaderText = String.Empty;
            _dgvtcName.HeaderText = "Name";
            _dgvtcSize.HeaderText = "Size";
            _dgvtcLastWriteTime.HeaderText = "LastWriteTime";


            this._dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                                        _dgvtcIsFile,
                                        _dgvtcName,
                                        _dgvtcSize,
                                        _dgvtcLastWriteTime});
        }
        #endregion

        #region public
        public void SetPresenter(Presenter thePresenter) 
        {
            _presenter = thePresenter;
        }
        public void Init() 
        {
            SetupColumns();
        }
        public void ResetData(string path, IList<DataDisplayObject> data) 
        {
            _tb.Text = path;
            _dgv.DataSource = data;

            if (String.IsNullOrEmpty(path))
                _btnUp.Enabled = false;
            else
                _btnUp.Enabled = true;
        }
        #endregion


        #region handlers
        private void _dgv_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            _presenter.DrillDownDesired(e.RowIndex);
        }
        private void _btnUp_Click(object sender, EventArgs e)
        {
            _presenter.UpDesired();
        }
        #endregion

    }
}
